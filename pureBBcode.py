import re, cgi, urlparse, posixpath

# Append directory for latest version of pygments! :-)
import sys
sys.path.insert(0, "./Pygments-1.6/")

import pygments
import pygments.lexers
import pygments.formatters
import pygments.styles

def htmlEscape(text):
	""" Escapes the given text, so we can embed it into the html code.
	"""
	return cgi.escape(text, quote=True)

def htmlSyntaxHighlighter(code, language, stylename='default',
	markedLines = None, firstLine = 1, showLineNumbers = True):
	""" Runs the pygments syntax highlighter for a given code. The result is html code which
		can be included directly into the website.
	"""

	try:
		lexer 		= pygments.lexers.get_lexer_by_name(language, encoding='utf-8')
		style 		= pygments.styles.get_style_by_name(stylename)

		if markedLines is None:
			markedLines = []

		# Workaround!
		for i, markedLine in enumerate(markedLines):
			markedLines[i] -= (firstLine - 1)

		kwds = {}

		kwds['hl_lines'] 	= markedLines
		kwds['linenostart']	= firstLine

		if showLineNumbers:
			kwds['linespans'] = 'codeline'
				
		formatter 	= pygments.formatters.HtmlFormatter( style=style, encoding='utf-8', cssclass="highlight-%s" % stylename, **kwds )
		html        = pygments.highlight(code, lexer, formatter)

		# Fix line number CSS stuff
		html = re.sub("<span id=\"codeline-([0-9]*)\">", lambda r: "<span class=\"codeline\" rel=\"%s\">" % r.group(1), html)

	except Exception, e: # Fallback - quick any dirty, but should work!
		html = "<code class=\"plaincode\">%s</code>" % htmlEscape(code)

	return html


class previewOutput(Exception):
	""" Exception used for the [preview]-tag, which returns only a part of the total bbcode.
		Use getHtml() to get the actual html code.
	"""

	def __init__(self, html):
		self.html = html

	def getHtml(self):
		return self.html

class invalidBBcode(Exception):
	""" Invalid BBcode
	"""
	def __init__(self):
		pass

class BBcode(object):
	""" The main BBcode parser class. This class contains functions to convert a code containing
		BBcode-tags into tokens, and moreover contains the necessary formatter functions to produce
		html output.
	"""
	TOKEN_DATA 			= 0
	TOKEN_TAG 			= 1

	URL_INVALID			= None
	URL_INTERNAL 		= 1
	URL_EXTERNAL 		= 2

	MARKER_TOC			= "<!-- [tableofcontents] //-->"

	def __init__(self, **kwds):
		self.tokens 			= []
		self.kwds   			= kwds

		self.sectionIndex 		= []
		self.tableOfContents 	= []

		# Read Variables
		self.previewMode 			= kwds['previewMode'] 			if kwds.has_key('previewMode') else None
		self.showTableOfContents 	= kwds['showTableOfContents']	if kwds.has_key('showTableOfContents') else None
		self.staticURL				= kwds['staticURL']				if kwds.has_key('staticURL') else None
		self.websiteURL				= kwds['websiteURL'] 			if kwds.has_key('websiteURL') else None
		self.lookupInternalURL 		= kwds['lookupInternalURL'] 	if kwds.has_key('lookupInternalURL') else None
		self.lookupTagURL			= kwds['lookupTagURL']			if kwds.has_key('lookupTagURL') else None
		self.internalHostnames   	= kwds['internalHostnames']		if kwds.has_key('internalHostnames') else None
		self.standaloneCSS 			= kwds['standaloneCSS']			if kwds.has_key('standaloneCSS') else None

		quotedStr   = "(\\\\.|[^\\\\\"])*"	#quotedStr 	= "((\\\\.)?[^\\\\\"]*)*"
		unquotedStr	= "[^\"\\s\\]]+"

		regExp_tag 	 = 	"\[(?P<close>/)?(?P<name>[a-zA-Z0-9*]+)"
		regExp_tag 	+=	"(=(\"(?P<quotedValue>%s)\"|(?P<value>%s)))?" % (quotedStr, unquotedStr)
		regExp_tag 	+= 	"(?P<arguments>(\\s+[a-zA-Z0-9]+(=(\"%s\"|%s))?)*)" % (quotedStr, unquotedStr)
		regExp_tag 	+=	"\\s*\]"

		regExp_attribute = "(?P<key>[a-zA-Z0-9]+)(=(\"(?P<quotedValue>%s)\"|(?P<value>%s)))?" % (quotedStr, unquotedStr)

		self.re_tag 		= re.compile(regExp_tag, re.DOTALL)
		self.re_attribute 	= re.compile(regExp_attribute, re.DOTALL)

		# Adapted from http://daringfireball.net/2010/07/improved_regex_for_matching_urls
		# Changed to only support one level of parentheses, since it was failing catastrophically on some URLs.
		# See http://www.regular-expressions.info/catastrophic.html
		self.re_url 		= re.compile(r'(?im)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\([^\s()<>]+\))+(?:\([^\s()<>]+\)|[^\s`!()\[\]{};:\'".,<>?]))')

		# Newline
		self.re_paragraph 	= re.compile("\n\s*\n")

	def checkURL(self, urlParts):
		if urlParts is None:
			return self.URL_INVALID

		if urlParts.netloc == '' and urlParts.scheme == '':
			return self.URL_INTERNAL

		# Is it contained in the list of internal hostnames
		if self.internalHostnames is not None:
			if urlParts.netloc in self.internalHostnames:
				return self.URL_INTERNAL

		return self.URL_EXTERNAL

	def tokenizer(self, code):
		""" Splits the code into DATA-parts (containing text) and TAG-parts (BBcode-tags).
			The splitting itself is done using a fancy regular expression.
			Unfortunately a regular expression doesn't save each occurance of a group, but instead
			only the last content of each group - so we have to do a second iteration to get key-value
			pairs.
		"""

		def __decodeValue__(r):
			if r.group('value') is not None:
				return r.group('value')
			elif r.group('quotedValue') is not None:
				
				# In case of incorrect escaped tags, just return the unescaped value to prevent exception
				try:
					return r.group('quotedValue').decode("string-escape", 'ignore')
				
				except ValueError:
					return r.group('quotedValue')

			else:
				return None

		lastTextPos = 0
		for r in self.re_tag.finditer(code):
			textInbetween 	= code[ lastTextPos: r.start(0) ]
			lastTextPos 	= r.end(0)

			if textInbetween != "":
				self.tokens.append((self.TOKEN_DATA, textInbetween))

			tag = {
				'__original__':	r.group(0),
				'__name__':		r.group('name'),
				'__close__':	r.group('close') is not None,
				'__value__':	__decodeValue__(r)
			}

			arguments = r.group('arguments')
			for r in self.re_attribute.finditer(arguments):
				key 		= r.group('key')
				tag[key] 	= __decodeValue__(r)
				
			self.tokens.append((self.TOKEN_TAG, tag))

		textInbetween = code[ lastTextPos: ]
		if textInbetween != "":
			self.tokens.append((self.TOKEN_DATA, textInbetween))

	def __cleanup__(self, data, formatEscape = True, formatGraphics = True, formatNewlines = True, formatURLs = True, formatBlockText = False):
		""" Given some data, this function performs the following steps depending on the given arguments.
			formatBlockText 	-- Searches for double linebreaks and converts them to </p>\n<p>
			formatURLs 			-- Searches for URLs directly in the text and converts them to HTML urls
								   (At least if the regular expression works correctly)
			formatEscape 		-- Escapes special characters like &, <, >
			formatGraphics 		-- (requires formatEscape) Replaces special character sequences with symbols / simileys
			formatNewlines		-- Replaces newlines with <br />
		"""

		def __formatEscape__(data):
			if formatEscape:
				data = htmlEscape(data)
				#for search, replace in [ ('&', '&amp;'), ('<', '&lt;'), ('>', '&gt;') ]:
				#	data = data.replace(search, replace)
				# ('-&gt;', '&#2192;'), ('&lt;-', '&#2190;'),

				if formatGraphics:
					for search, replace in [
							('---', '&mdash;'), ('--', '&ndash;'), ('...', '&#8230;'), 
							('(c)', '&copy;'), ('(reg)', '&reg;'), ('(tm)', '&trade;') ]:
						data = data.replace(search, replace)

					for search, replace in [
							('8-)', 'cool.png'),
							('$-)', 'greedy.png'),
							(':-(', 'sad.png'),
							(':-)', 'smile.png'),
							(':-|', 'speechless.png'),
							(':-P', 'tongue.png'),
							(':-/', 'unsure.png'),
							(';-)', 'wink.png') ]:

						imageURL = "images/smileys/%s" % replace

						if self.staticURL:
							imageURL = urlparse.urljoin( self.staticURL, imageURL )

						urlEscaped = htmlEscape(imageURL)
						data = data.replace(search, "<img src=\"%s\" alt=\"%s\" />" % (urlEscaped, search) )

			if formatNewlines:
				data = data.replace('\n', '<br />\n')

			return data

		def __formatURLs__(data):
			if not formatURLs:
				return __formatEscape__(data)

			html = ""

			lastTextPos = 0
			for r in self.re_url.finditer(data):
				url 	= r.group(0)

				try:
					urlParts = urlparse.urlparse(url)
				except ValueError:
					urlParts = None

				check = self.checkURL(urlParts)
				if check == self.URL_INVALID: # invalid URL, do not parse now!
					continue # dont set lastTextPos!

				textInbetween 	= data[ lastTextPos: r.start(0) ]
				lastTextPos 	= r.end(0)

				if textInbetween != "":
					html += __formatEscape__(textInbetween)

				# Generate attributes for this link
				htmlAttributes = ""
				if check != self.URL_INTERNAL:
					htmlAttributes += " class=\"extern\" target=\"_blank\""

				# Add this link to the data
				urlEscaped = htmlEscape(url)
				html += "<a%s href=\"%s\">%s</a>" % (htmlAttributes, urlEscaped, urlEscaped)

			textInbetween = data[ lastTextPos: ]
			if textInbetween != "":
				html += __formatEscape__(textInbetween)

			return html

		def __formatBlockText__(data):
			if not formatBlockText:
				return __formatURLs__(data)

			html = ""

			lastTextPos = 0
			i           = 0
			for r in self.re_paragraph.finditer(data):
				textInbetween 	= data[ lastTextPos: r.start(0) ]
				lastTextPos 	= r.end(0)

				textInbetween   = textInbetween.rstrip()

				if i != 0:
					textInbetween = textInbetween.lstrip()
				if textInbetween != "":
					html += __formatURLs__(textInbetween)

				html 	+= "</p>\n<p>"
				i 		+= 1

			textInbetween = data[ lastTextPos: ]

			if i != 0:
				textInbetween = textInbetween.lstrip()
			if textInbetween != "":
				html += __formatURLs__(textInbetween)

			return html

		return __formatBlockText__(data)


	def format(self):
		""" Wrapper function to format stuff and return HTML output! """

		def __getTOC__(r):

			def __section__(sectionIndex = None):
				if sectionIndex is None:
					sectionIndex = []

				html = "<ul>\n"

				while len(self.tableOfContents):
					index, htmlAnchor, htmlTitle = self.tableOfContents[0]

					if len(index) < len(sectionIndex):
						break
					if index[:len(sectionIndex)] != sectionIndex:
						break

					# Remove the element
					self.tableOfContents.pop(0)
					htmlIndex = htmlEscape(".".join([str(i) for i in index]))

					html += "<li><a href=\"#%s\">%s. %s</a>" % (htmlAnchor, htmlIndex, htmlTitle)
					html += __section__(index)
					html += "</li>\n"

				html += "</ul>"
				return html

			# Only one tableOfContents allowed, otherwise return bbcode!
			if len(self.tableOfContents) == 0:
				return self.MARKER_TOC

			html = "<div class=\"tableofcontents\">"
			html += "<h1>Table of contents</h1>\n"
			html += __section__()
			html += "</div>"
			return html

		# TOC
		self.sectionIndex 		= []
		self.tableOfContents 	= []
		#self.previewText        = None

		# Render html
		try:
			html = self.__format__()
		except previewOutput, p:
			html = p.getHtml()
		
		# If we should not renter the TOC, just leave it blank
		if self.showTableOfContents is not None:
			if not self.showTableOfContents:
				self.tableOfContents = []

		# TOC has still to be rendered?	
		html = re.sub(re.escape(self.MARKER_TOC), __getTOC__, html)

		return html

	def __format__(self, 					\
		expectedCloseTag 	= None, 	\
		noEmbedded 			= False, 	\
		formatEscape        = True,		\
		formatGraphics      = True, 	\
		formatNewlines 		= True, 	\
		formatURLs 			= True,		\
		stripChars          = None ):
		""" Does the actually formatting stuff and returns HTML output! """
		
		# Should all paragraphs be splitted and placed into separate <p>-tags?
		formatBlockText 	= 	formatNewlines and \
								expectedCloseTag in (None, 'indent', 'preview', 'quote', 'center', 'warning', 'info', 'technical', 'todo', 'faq')

		# Final close tag which will be appended at the end!
		formatBlockTag 		= 	[None]

		output 			= []	# Not concatenated yet
		htmlOutput		= []

		def __flushOutput__(closeTag = False):

			# Strip on left side
			while len(output):
				tokenType, tokenValue = output[0]
				if tokenType != self.TOKEN_DATA:
					break

				tokenValue = tokenValue.lstrip(stripChars)
				if tokenValue == "":
					output.pop(0)
					continue

				output[0] = (tokenType, tokenValue)
				break

			# Strip on right side
			while len(output):
				tokenType, tokenValue = output[-1]
				if tokenType != self.TOKEN_DATA:
					break

				tokenValue = tokenValue.rstrip(stripChars)
				if tokenValue == "":
					output.pop()
					continue

				output[-1] = (tokenType, tokenValue)
				break

			# No output, no html! :)
			if len(output) == 0:
				
				# Final close tag?
				if closeTag and formatBlockTag[0]:
					htmlOutput.append(formatBlockTag[0])
					formatBlockTag[0] = None

				return

			# Is this blocktext ?
			if formatBlockText:
				output.insert(0, (self.TOKEN_TAG, "<p>"))
				output.append((self.TOKEN_TAG, "</p>\n"))

			# Final close tag?
			if closeTag and formatBlockTag[0]:
				output.append((self.TOKEN_TAG, formatBlockTag[0]))
				formatBlockTag[0] = None

			for tokenType, tokenValue in output:
				if tokenType == self.TOKEN_DATA:
					htmlOutput.append( self.__cleanup__(tokenValue, formatEscape=formatEscape, formatGraphics=formatGraphics,
						formatNewlines=formatNewlines, formatURLs=formatURLs, formatBlockText=formatBlockText) )
				else:
					htmlOutput.append( tokenValue )

			# Delete all output!
			del output[:]

			# Ready
			pass

		def __outputOutline__(html):
			if not formatBlockText:
				output.append((self.TOKEN_TAG, html ))
				return

			__flushOutput__()
			htmlOutput.append( html )
			pass

		def __outputInline__(html):
			output.append((self.TOKEN_TAG, html ))

		def __outputAny__(inline, html):
			if inline:
				__outputInline__(html)
			else:
				__outputOutline__(html)

		# Parse tokens
		while len(self.tokens):
			tokenType, tokenValue = self.tokens.pop(0)

			if tokenType == self.TOKEN_DATA:
				output.append((self.TOKEN_DATA, tokenValue))

			elif tokenType == self.TOKEN_TAG:
				tagName 	= tokenValue['__name__']
				tagClose 	= tokenValue['__close__']
				tagValue    = tokenValue['__value__']

				# This is a close tag
				if tagClose:
					if tagName != expectedCloseTag:
						output.append((self.TOKEN_DATA, tokenValue['__original__']))
						continue

					# Correct close tag, return html content
					break

				if noEmbedded: # No embedded tags allowed
					output.append((self.TOKEN_DATA, tokenValue['__original__']))
					continue

				# Try to parse this BBcode tag (possible exception: invalidBBcode
				try:

					if tagName == 'b':	# bold
						__outputInline__("<strong>%s</strong>" % self.__format__(tagName) )

					elif tagName == 'i': # italic
						__outputInline__("<em>%s</em>" % self.__format__(tagName) )

					elif tagName == 'u': # underline
						__outputInline__("<u>%s</u>" % self.__format__(tagName) )

					elif tagName == 's': # strike
						__outputInline__("<strike>%s</strike>" % self.__format__(tagName) )

					elif tagName == 'color': # color
						if tagValue is None:
							raise invalidBBcode()

						__outputInline__("<span style=\"color:%s;\">%s</span>" % (tagValue, self.__format__(tagName)) )

					elif tagName == 'size': # size
						if tagValue is None:
							raise invalidBBcode()

						__outputInline__("<span style=\"font-size:%s;\">%s</span>" % (tagValue, self.__format__(tagName)) )

					elif tagName == "sup": # ^
						__outputInline__("<sup>%s</sup>" % self.__format__(tagName) )

					elif tagName == "sub": # _
						__outputInline__("<sub>%s</sub>" % self.__format__(tagName) )

					elif tagName == 'indent': # indent
						if tagValue is None:
							raise invalidBBcode()

						__outputOutline__("<div style=\"margin-left:%s;\">%s</span>" % (tagValue, self.__format__(tagName)) )

					elif tagName == 'br':
						__outputInline__("<br />")

					elif tagName == 'quote': # quote
						author 		= tagValue
						htmlAuthor 	= ""

						if author is not None:
							url 	= tokenValue['url'] if tokenValue.has_key('url') else None
							htmlAuthor 	= "<cite>Quote by "

							if url is not None:
								urlEscaped = htmlEscape(url)
								htmlAuthor += "<a href=\"%s\">" % urlEscaped

							htmlAuthor += htmlEscape(author)

							if url is not None:
								htmlAuthor += "</a>"

							htmlAuthor += "</cite>\n"

						__outputOutline__("<blockquote>%s%s</blockquote>" % (htmlAuthor, self.__format__(tagName)))

					elif tagName == 'center':
						__outputOutline__("<div style=\"text-align:center;\">%s</div>" % self.__format__(tagName) )

					elif tagName == 'code':	# code
						language 		= tagValue
						stylename  		= tokenValue['style'] if tokenValue.has_key('style') else "default"

						try:
							markedLines 	= [int(lineNumber) for lineNumber in tokenValue['mark'].split(',')] if tokenValue.has_key('mark') else None
						except ValueError:
							markedLines 	= None

						try:
							firstLine       = int(tokenValue['firstline']) if tokenValue.has_key('firstline') else 1
						except ValueError:
							firstLine 		= 1

						showLineNumbers = not tokenValue.has_key('nolinenumbers')

						if language is not None:
							code 		= self.__format__(tagName, noEmbedded=True, formatEscape=False, formatNewlines=False, formatURLs=False, stripChars='\r\n')
							__outputOutline__( htmlSyntaxHighlighter(code, language, stylename, markedLines, firstLine, showLineNumbers) )

						else:
							__outputInline__( "<code class=\"plaincode\">%s</code>" % self.__format__(tagName, noEmbedded=True, formatGraphics=False, formatNewlines=False, formatURLs=False, stripChars='\r\n') )

					elif tagName == 'noparse': # dont parse embedded bb codes
						__outputInline__( self.__format__(tagName, noEmbedded=True, formatGraphics=False, formatNewlines=False, formatURLs=False) )

					elif tagName == 'comment': # dont parse embedded bb codes and drop the result ^^
						self.__format__(tagName, noEmbedded=True, formatGraphics=False, formatNewlines=False, formatURLs=False)

					elif tagName == 'math' or tagName == 'imath':
						inline 			= (tagName == 'imath')
						htmlAttributes 	= "" if inline else "; mode=display"

						if self.standaloneCSS:
							__outputAny__(inline, "<code>[Formula cannot be displayed in RSS Feed]</code>")
						else:
							__outputAny__(inline, "<script type=\"math/tex%s\">%s</script>" % (htmlAttributes, self.__format__(tagName, noEmbedded=True, formatEscape=False, formatNewlines=False, formatURLs=False, stripChars='\r\n')) )
							__outputAny__(inline, "<noscript><code>[Please enable JavaScript to see the formula]</code></noscript>")

					elif tagName == 'url' or tagName == 'aurl': # url
						url 		= tagValue
						description = None

						if url is not None:
							description = self.__format__(tagName, formatURLs=False)
						else:
							url 		= self.__format__(tagName, noEmbedded=True, formatEscape=False,	formatNewlines=False, formatURLs=False)

						# Split the URL
						try:
							urlParts = urlparse.urlparse(url)
						except ValueError:
							urlParts = None

						# Append / Replace anchor if given
						if urlParts is not None and tokenValue.has_key('anchor') and tokenValue['anchor'] is not None:
							urlParts 	= urlParts._replace(fragment=tokenValue['anchor'])
							url 		= urlparse.urlunparse(urlParts)

						# Resolve internal URL
						if tagName == 'aurl' and self.lookupInternalURL is not None:
							
							# aurl doesn't work with complete hostnames
							if urlParts is None or urlParts.netloc != '' or urlParts.scheme != '':
								raise invalidBBcode()

							try:
								urlParts 	= urlParts._replace( path=self.lookupInternalURL(urlParts.path))
								url 		= urlparse.urlunparse(urlParts)
							except KeyError:
								raise invalidBBcode()

						# Append website URL
						if 		urlParts is not None and urlParts.scheme == '' and urlParts.netloc == '' and \
								urlParts.path != '' and self.websiteURL is not None:
							url = urlparse.urljoin( self.websiteURL, url )

						# Get attributes
						htmlAttributes = ""

						if self.checkURL(urlParts) != self.URL_INTERNAL:
							htmlAttributes += " class=\"extern\" target=\"_blank\""
						elif tokenValue.has_key('newwindow'):
							htmlAttributes += " target=\"_blank\""

						# Escape URL and get description
						urlEscaped = htmlEscape(url)

						if description is None:
							description = urlEscaped

						__outputInline__( "<a%s href=\"%s\">%s</a>" % (htmlAttributes, urlEscaped, description) )

					elif tagName == 'tag': # tag
						tag 		= self.__format__(tagName, noEmbedded=True, formatEscape=False,	formatNewlines=False, formatURLs=False)

						# Resolve internal URL
						if self.lookupTagURL is None:
							raise invalidBBcode()
						
						try:
							url 		= self.lookupTagURL(tag)
							urlParts 	= urlparse.urlparse(url)
						except (KeyError, ValueError):
							raise invalidBBcode()

						# Append website URL
						if 		urlParts is not None and urlParts.scheme == '' and urlParts.netloc == '' and \
								urlParts.path != '' and self.websiteURL is not None:
							url = urlparse.urljoin( self.websiteURL, url )

						# Escape URL and get description
						urlEscaped = htmlEscape(url)

						__outputInline__( "<a class=\"linktag\" href=\"%s\"><span class=\"tag\">%s</span></a>" % (urlEscaped, tag) )


					elif tagName == 'img' or tagName == 'aimg': # image
						url 		= tagValue
						previewURL  = None
						description = None

						if url is not None:
							description = self.__format__(tagName, formatURLs=False)
						else:
							url 		= self.__format__(tagName, noEmbedded=True, formatEscape=False,	formatNewlines=False, formatURLs=False)

						# Split the URL
						try:
							urlParts = urlparse.urlparse(url)
						except ValueError:
							urlParts = None

						# preview URL
						previewURL = url

						# Resolve internal URL
						if tagName == 'aimg' and self.lookupInternalURL is not None:

							# aimg doesn't work with complete hostnames
							if urlParts is None or urlParts.scheme != '' or urlParts.netloc != '':
								raise invalidBBcode()

							try:
								previewURL  = urlparse.urlunparse(urlParts._replace( path=self.lookupInternalURL(previewURL, tokenValue) ))
								urlParts    = urlParts._replace( path=self.lookupInternalURL(url) )
								url         = urlparse.urlunparse(urlParts)

							except KeyError:
								raise invalidBBcode()

						# Append website URL
						if 		urlParts is not None and urlParts.scheme == '' and urlParts.netloc == '' and \
								urlParts.path != '' and self.websiteURL is not None:
							url 		= urlparse.urljoin( self.websiteURL, url )
							previewURL 	= urlparse.urljoin( self.websiteURL, previewURL )

						# Hardcoded size?
						# NOTE: This is very tricky! The lookupInternalURL command removes
						# the width and height if it was used to prevent multiple resizes!
						#htmlSize = ""
						#if tokenValue.has_key('width') and tokenValue['width'] is not None:
						#	htmlSize += " width=\"%s\"" % htmlEscape(tokenValue['width'])
						#if tokenValue.has_key('height') and tokenValue['height'] is not None:
						#	htmlSize += " height=\"%s\"" % htmlEscape(tokenValue['height'])

						htmlAttributes 	= ""
						imageFloat  	= tokenValue['float'] if tokenValue.has_key('float') else None
						if (self.standaloneCSS and description is not None) or imageFloat in ('left', 'right') or \
							(tokenValue.has_key('width') and tokenValue['width'] is not None) or \
							(tokenValue.has_key('height') and tokenValue['height'] is not None):

							htmlAttributes = " style=\""

							if imageFloat == 'left':
								htmlAttributes += "float:left;"
							elif imageFloat == 'right':
								htmlAttributes += "float:right;"

							if self.standaloneCSS and description is not None:
								htmlAttributes += "display:inline-block; background-color:#eee; padding:1.5ex; border-radius:1.5ex; border:1px solid #D7D7D7; text-align:center;"

							if tokenValue.has_key('width') and tokenValue['width'] is not None:
								width = tokenValue['width']
								if width.isdigit():
									width += "px"
								htmlAttributes += "width:%s;" % htmlEscape(width)

							if tokenValue.has_key('height') and tokenValue['height'] is not None:
								height = tokenValue['height']
								if height.isdigit():
									height += "px"
								htmlAttributes += "height:%s;" % htmlEscape(height)

							htmlAttributes += "\""

						# By default all images are assigned to the same group
						imageGroup 	= tokenValue['group'] if tokenValue.has_key('group') else None
						if imageGroup is None:
							imageGroup = "default"

						# Escape URL and get description
						urlEscaped 			= htmlEscape(url)
						previewURLEscaped 	= htmlEscape(previewURL)

						if description is not None:

							htmlAttributesDescription = ""

							if self.standaloneCSS or not tokenValue.has_key('nomaxwidth'):
								htmlAttributesDescription = " style=\""

								if not tokenValue.has_key('nomaxwidth'):
									maxWidth = None

									if tokenValue.has_key('image-width') and tokenValue['image-width'] is not None:
										maxWidth = tokenValue['image-width']
									elif tokenValue.has_key('width') and tokenValue['width'] is not None:
										maxWidth = tokenValue['width']
									
									if maxWidth is not None:
										if maxWidth.isdigit():
											maxWidth += "px"
										htmlAttributesDescription += "display: block; max-width: %s;" % htmlEscape(maxWidth)

								if self.standaloneCSS:
									htmlAttributesDescription += "padding-top:1ex; color:gray;"

								htmlAttributesDescription += "\""

							__outputOutline__("<div class=\"imagecontent\"%s><a href=\"%s\" rel=\"lightbox[%s]\" title=\"%s\"><img class=\"source\" src=\"%s\" alt=\"%s\" /></a><span class=\"description\"%s>%s</span></div>" % \
									(htmlAttributes, urlEscaped, htmlEscape(imageGroup), htmlEscape(description), previewURLEscaped, htmlEscape(description), htmlAttributesDescription, description) )
						
						else:
							__outputInline__("<img class=\"source\" src=\"%s\" alt=\"\"%s />" % (previewURLEscaped, htmlAttributes) )

					elif tagName == 'list': # list
						cssAttributes = {
								'1': 'decimal', '01': 'decimal-leading-zero',
								'a': 'lower-alpha', 'A': 'upper-alpha',
								'i': 'lower-roman', 'I': 'upper-roman',
							}

						tag = 'ol' if tagValue in cssAttributes else 'ul'

						htmlAttributes = (" style=\"list-style-type:%s;\"" % cssAttributes[tagValue]) if tagValue in cssAttributes else ""
						if tokenValue.has_key('space'):
							htmlAttributes += " class=\"listspace\""

						__outputOutline__("<%s%s>%s</%s>" % (tag, htmlAttributes, self.__format__(tagName), tag) )

					elif tagName == 'preview':
						htmlPreview	= self.__format__(tagName)

						if self.previewMode:
							raise previewOutput( htmlPreview )
						else:
							__outputOutline__(htmlPreview )

					elif tagName == 'warning':
						imageURL = "images/warning.svg"

						if self.staticURL is not None:
							imageURL = urlparse.urljoin( self.staticURL, imageURL )

						urlEscaped = htmlEscape(imageURL)

						if self.standaloneCSS:
							__outputOutline__("<div style=\"background-color:#F6CECE; border-radius:1.5ex; overflow:auto; margin:1em; padding:0 1em 0 1em;\"><img style=\"float:left; margin:1em 1em 0 0;\" src=\"%s\" alt=\"warning\" \><p><b>Warning:</b></p>%s</div>" % (urlEscaped, self.__format__(tagName)) )
						else:
							__outputOutline__("<div class=\"warningarea\"><img class=\"sign\" src=\"%s\" alt=\"warning\" \><p><b>Warning:</b></p>%s</div>" % (urlEscaped, self.__format__(tagName)) )

					elif tagName == 'info':
						imageURL = "images/info.svg"

						if self.staticURL is not None:
							imageURL = urlparse.urljoin( self.staticURL, imageURL )

						urlEscaped = htmlEscape(imageURL)

						if self.standaloneCSS:
							__outputOutline__("<div style=\"background-color:#CEE3F6; border-radius:1.5ex; overflow:auto; margin:1em; padding:0 1em 0 1em;\"><img style=\"float:left; margin:1em 1em 0 0;\" src=\"%s\" alt=\"info\" \><p><b>Info:</b></p>%s</div>" % (urlEscaped, self.__format__(tagName)) )
						else:
							__outputOutline__("<div class=\"infoarea\"><img class=\"sign\" src=\"%s\" alt=\"info\" \><p><b>Info:</b></p>%s</div>" % (urlEscaped, self.__format__(tagName)) )

					elif tagName == 'technical':
						imageURL = "images/technical.svg"

						if self.staticURL is not None:
							imageURL = urlparse.urljoin( self.staticURL, imageURL )

						urlEscaped = htmlEscape(imageURL)

						if self.standaloneCSS:
							__outputOutline__("<div style=\"background-color:#F2F2F2; border-radius:1.5ex; overflow:auto; margin:1em; padding:0 1em 0 1em;\"><img style=\"float:left; margin:1em 1em 0 0;\" src=\"%s\" alt=\"technical details\" \><p><b>Technical details:</b></p>%s</div>" % (urlEscaped, self.__format__(tagName)) )
						else:
							__outputOutline__("<div class=\"technicalarea\"><img class=\"sign\" src=\"%s\" alt=\"technical details\" \><p><b>Technical details:</b></p>%s</div>" % (urlEscaped, self.__format__(tagName)) )

					elif tagName == 'todo':
						imageURL = "images/todo.svg"

						if self.staticURL is not None:
							imageURL = urlparse.urljoin( self.staticURL, imageURL )

						urlEscaped = htmlEscape(imageURL)

						if self.standaloneCSS:
							__outputOutline__("<div style=\"background-color:#F2F2F2; border-radius:1.5ex; overflow:auto; margin:1em; padding:0 1em 0 1em;\"><img style=\"float:left; margin:1em 1em 0 0;\" src=\"%s\" alt=\"todo\" \><p><b>Todo:</b></p>%s</div>" % (urlEscaped, self.__format__(tagName)) )
						else:
							__outputOutline__("<div class=\"todoarea\"><img class=\"sign\" src=\"%s\" alt=\"todo\" \><p><b>Todo:</b></p>%s</div>" % (urlEscaped, self.__format__(tagName)) )

					elif tagName == 'download':
						imageURL = "images/download.svg"

						if self.staticURL is not None:
							imageURL = urlparse.urljoin( self.staticURL, imageURL )

						urlEscaped = htmlEscape(imageURL)
						__outputOutline__("<div class=\"downloadarea\"><img class=\"sign\" src=\"%s\" alt=\"download\" \><div class=\"downloadcontent\"><p><b>Download:</b></p><ul>%s</ul></div></div>" % (urlEscaped, self.__format__(tagName)) )

					elif tagName == 'faq':
						__outputOutline__("<div class=\"faq\">%s</div>" % self.__format__(tagName) )

					elif tagName == 'section' or tagName == 'subsection' or tagName == 'subsubsection':
						depth, tag, htmlStandaloneStyle = {
								'section': 			(1, 'h1', " style=\"font-size:1.2em; color:black; font-weight:bold;\""),
								'subsection': 		(2, 'h2', " style=\"font-size:1.1em; color:black; font-weight:bold;\""),
								'subsubsection': 	(3, 'h3', " style=\"font-size:1.1em; color:black; font-weight:bold;\"")
							}[tagName]

						# Set to specific section/subsection/subsubsection
						if tagValue is not None:
							try:
								self.sectionIndex = [int(i) for i in tagValue.split('.')]
							except ValueError: # Incorrect index
								self.sectionIndex = []

						# Append with .1 for each subsection
						if len(self.sectionIndex) < depth:
							while len(self.sectionIndex) < depth:
								self.sectionIndex.append( 1 )

						# Increment by one if no explicit tag was given
						elif tagValue is None:
							self.sectionIndex = self.sectionIndex[:depth]
							self.sectionIndex[depth-1] += 1

						htmlIndex = htmlEscape(".".join([str(i) for i in self.sectionIndex]))
						htmlTitle = self.__format__(tagName)
						htmlAnchor = "section_%s" % htmlEscape("_".join([str(i) for i in self.sectionIndex]))

						self.tableOfContents.append((self.sectionIndex[:], htmlAnchor, htmlTitle))

						if self.standaloneCSS:
							__outputOutline__("<%s%s>%s. %s</%s>\n" % (tag, htmlStandaloneStyle, htmlIndex, htmlTitle, tag) )
						elif self.previewMode:
							__outputOutline__("<%s>%s. %s</%s>\n" % (tag, htmlIndex, htmlTitle, tag) )
						else:
							__outputOutline__("<%s id=\"%s\">%s. %s</%s>\n" % (tag, htmlAnchor, htmlIndex, htmlTitle, tag) )

					elif tagName == 'table':
						htmlAttributes = ""
						if tokenValue.has_key('width') and tokenValue['width'] is not None:
							htmlAttributes += " width=\"%s\"" % htmlEscape(tokenValue['width'])
						if tokenValue.has_key('height') and tokenValue['height'] is not None:
							htmlAttributes += " height=\"%s\"" % htmlEscape(tokenValue['height'])

						if self.standaloneCSS or tokenValue.has_key('center') or tokenValue.has_key('nominwidth'):
							htmlAttributes += " style=\""
							if tokenValue.has_key('center'):
								htmlAttributes += "margin:auto;"
							if tokenValue.has_key('nominwidth'):
								htmlAttributes += "min-width:0;"
							if self.standaloneCSS:
								htmlAttributes += "background-color:#eee; padding:1.5ex; border-radius:1.5ex; border:1px solid #D7D7D7;"
							htmlAttributes += "\""

						htmlClasses = []

						if not tokenValue.has_key('nobox'):
							htmlClasses.append("tablebox")
						if tokenValue.has_key('float'):
							htmlClasses.append("tablefloat")
						if tokenValue.has_key('floatleft'):
							htmlClasses.append("tablefloatleft")
						if len(htmlClasses):
							htmlAttributes += " class=\"%s\"" % ' '.join(htmlClasses)

						__outputOutline__("<table%s>%s</table>" % (htmlAttributes, self.__format__(tagName)) )

					elif tagName == 'row':
						htmlAttributes = ""
						if tokenValue.has_key('height') and tokenValue['height'] is not None:
							htmlAttributes += " height=\"%s\"" % htmlEscape(tokenValue['height'])

						__flushOutput__()
						htmlOutput.append("<tr%s>%s</tr>" % (htmlAttributes, self.__format__(tagName)) )

					elif tagName == 'tableofcontents':
						__outputOutline__( "" if self.previewMode else ("%s\n" % self.MARKER_TOC) )

					elif tagName == 'rating':
						__outputOutline__("<table class=\"rating\">%s</table>" % self.__format__(tagName) )

					elif tagName == 'anchor':
						if tagValue is not None:
							if not self.previewMode:
								anchorEscaped = htmlEscape(tagValue)
								__outputOutline__("<span id=\"%s\" /></span>" % anchorEscaped)
							elif formatBlockText:
								__flushOutput__()

					elif tagName == '*' and expectedCloseTag == 'list':
						__flushOutput__( closeTag = True )
						htmlOutput.append("<li>")
						formatBlockTag = ["</li>\n"]

					elif tagName == '*' and expectedCloseTag == 'download':
						html 		= ""
						filename 	= tokenValue['name'] if tokenValue.has_key('name') else None

						if tokenValue.has_key('url'):
							urlEscaped = htmlEscape( tokenValue['url'] )
							if filename is None:
								filename = posixpath.basename(urlparse.urlparse( tokenValue['url'] ).path)
							escapedFilename = htmlEscape( filename )
							html += "<div class=\"fileurl\"><a href=\"%s\">%s</a></div>" % (urlEscaped, escapedFilename)
						if tokenValue.has_key('md5'):
							escapedMd5 = htmlEscape( tokenValue['md5'] )
							html += "<div class=\"filedigest\">MD5:&nbsp;&nbsp;&nbsp; %s</div>" % escapedMd5
						if tokenValue.has_key('sha256'):
							escapedMd5 = htmlEscape( tokenValue['sha256'] )
							html += "<div class=\"filedigest\">SHA256: %s</div>" % escapedMd5
						if tokenValue.has_key('size'):
							escapedMd5 = htmlEscape( tokenValue['size'] )
							html += "<div class=\"filesize\">Size:&nbsp;&nbsp; %s</div>" % escapedMd5

						__flushOutput__( closeTag = True )
						htmlOutput.append("<li>%s<div class=\"filedescription\">" % html)
						formatBlockTag = ["</div></li>\n"]

					elif tagName == '*' and expectedCloseTag == 'faq':
						question		= tagValue
						htmlQuestion 	= ""

						if question is not None:
							htmlQuestion = "<div class=\"question\">"
							htmlQuestion += htmlEscape(question)
							htmlQuestion += "</div>\n"

						__flushOutput__( closeTag = True )
						htmlOutput.append("%s<div class=\"answer\">" % htmlQuestion)
						formatBlockTag = ["</div>\n"]

					elif tagName == '*' and expectedCloseTag == 'row':
						htmlAttributes = ""

						if tokenValue.has_key('width') and tokenValue['width'] is not None:
							htmlAttributes += " width=\"%s\"" % htmlEscape(tokenValue['width'])
						if tokenValue.has_key('height') and tokenValue['height'] is not None:
							htmlAttributes += " height=\"%s\"" % htmlEscape(tokenValue['height'])
						if tokenValue.has_key('rowspan') and tokenValue['rowspan'] is not None:
							htmlAttributes += " rowspan=\"%s\"" % htmlEscape(tokenValue['rowspan'])
						if tokenValue.has_key('colspan') and tokenValue['colspan'] is not None:
							htmlAttributes += " colspan=\"%s\"" % htmlEscape(tokenValue['colspan'])

						if tokenValue.has_key('nowrap'):
							htmlAttributes += " style=\"white-space: nowrap\""

						if tokenValue.has_key('center'):
							htmlAttributes += " style=\"text-align: center\""

						__flushOutput__( closeTag = True)
						htmlOutput.append("<td%s>" % htmlAttributes)
						formatBlockTag = ["</td>\n"]

					elif tagName == '*' and expectedCloseTag == 'rating':
						try:
							rating      = int(tagValue) if tagValue is not None else 1
						except ValueError:
							rating 		= 1

						if rating < 1:
							rating = 1
						elif rating > 5:
							rating = 5

						# Get image url
						imageURL = "images/stars/star%d.svg" % rating

						if self.staticURL is not None:
							imageURL = urlparse.urljoin( self.staticURL, imageURL )

						urlEscaped = htmlEscape(imageURL)

						htmlAttributes = ""
						if self.standaloneCSS:
							htmlAttributes = " style=\"height:1em; margin-left:1ex;\""

						# Flush output
						__flushOutput__( closeTag = True )
						htmlOutput.append("<tr><td>")
						formatBlockTag = [":</td><td><img class=\"stars\" src=\"%s\" alt=\"%d/5\"%s \></td></tr>\n" % (urlEscaped, rating, htmlAttributes)]

					# Unrecognized tag, just print original data
					else:
						raise invalidBBcode()

				except invalidBBcode:
					output.append((self.TOKEN_DATA, tokenValue['__original__'] ))
					continue

		# Flush the output!
		__flushOutput__( closeTag = True )

		return ''.join(htmlOutput)

def htmlBBcode(code, previewMode=False, showTableOfContents=True, staticURL="/", websiteURL="/", lookupInternalURL=None, lookupTagURL=None, internalHostnames=None, standaloneCSS=False):
	""" PureFans (ultimate) BBCode parser

		previewMode 		-- If True only the part inbetween of [preview]...[/preview] is returned
		websiteURL			-- URL of this website - required to correctly link to smileys and relative files
		lookupInternalURL	-- Required for [aurl] and [aimg] which also accept source filenames
		internalHostnames   -- Required to check which URls are local and which are remote ones 

	"""
	parser = BBcode( 	previewMode			= previewMode, 			\
						showTableOfContents = showTableOfContents, 	\
						staticURL 			= staticURL,			\
						websiteURL			= websiteURL, 			\
						lookupInternalURL	= lookupInternalURL, 	\
						lookupTagURL 		= lookupTagURL,			\
						internalHostnames	= internalHostnames,	\
						standaloneCSS		= standaloneCSS
					)
	parser.tokenizer(code)
	return parser.format()

# Escapes BBcode
def noparseBBcode(code):
	parser = BBcode()
	parser.tokenizer(code)

	noparse_tags = False
	output = ""

	while len(parser.tokens):
		tokenType, tokenValue = parser.tokens.pop(0)

		if tokenType == parser.TOKEN_DATA:
			output += tokenValue

		elif tokenType == parser.TOKEN_TAG:
			noparse_tags = True

			if tokenValue['__name__'] == "noparse" and tokenValue['__close__']:
				output += "[[/noparse][noparse]%s" % tokenValue['__original__'][1:]
			else:
				output += tokenValue['__original__']

	if noparse_tags:
		output = "[noparse]%s[/noparse]" % output

	return output
