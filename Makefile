.PHONY: all
all: cms

.PHONY: cms
cms:
	python dpcms.py

.PHONY: clean
clean:
	find source -name "*.copy-image" -type f -delete
	cd cms && git clean -f -d -x
