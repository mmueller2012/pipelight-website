# -*- coding: utf-8 -*-

import os, os.path, hashlib, operator, sys, re 		# file, stdinout and regexp
import itertools 									# islice
import datetime 									# datetime of articles
import dateutil.tz 									# for tzlocal
import dateutil.parser								# better way to parse datetimes from files
import cgi 											# for escaping
import urlparse, posixpath 							# parsing urls
import shutil 										# copying files
import httplib										# invaliding cache
import threading 									# Locking mechanism
from contextlib import contextmanager 				# contextmanager
import subprocess									# image converts, send mail etc.
import socket										# socket.gaierror
import errno										# Error codes
import struct 										# Extract metadata for image
import pureBBcode 									# BBcode parser
import config 										# CMS config
from builder import buildObject, buildList, buildFile, buildRawData, buildTemplate # Template system

def getImageInfo(filename):
	""" Tries to extract the image size from the header
	"""

	# This is a bit tricky, but we need a method to find the dimensions of the original
	# image. The implementation here is based on:
	# https://code.google.com/p/bfg-pages/source/browse/trunk/pages/getimageinfo.py

	image_type 		= None
	image_width 	= None
	image_height 	= None

	try:

		with open(filename) as fp:
			data = fp.read(24)

			if len(data) >= 10 and data[:6] in ('GIF87a', 'GIF89a'):
				image_type = 'gif'
				image_width, image_height = struct.unpack("<HH", data[6:10])

			elif len(data) >= 24 and data.startswith('\211PNG\r\n\032\n') and data[12:16] == 'IHDR':
				image_type = 'png'
				image_width, image_height = struct.unpack(">LL", data[16:24])

			elif len(data) >= 16 and data.startswith('\211PNG\r\n\032\n'):
				image_type = 'png'
				image_width, image_height = struct.unpack(">LL", data[8:16])

			elif len(data) >= 3 and data.startswith('\377\330'):
				image_type = 'jpeg'

				fp.seek(2)

				while True:
					b = fp.read(1)
					if b == '' or b == '\xDA':
						break

					# Skip still \xFF
					while b != '' and b != '\xFF':
						b = fp.read(1)

					# As long as there is \xFF
					while b == '\xFF':
						b = fp.read(1)

					if b in ('\xC0', '\xC1', '\xC2', '\xC3'):
						fp.read(3)
						image_height, image_width = struct.unpack(">HH", fp.read(4))
						break

					elif b != '':
						l, = struct.unpack(">H", fp.read(2))
						if l <= 2: # Prevent endless loop!
							break
						fp.seek(l-2, 1) # jump relative to current position

					else: # b == ''
						break

			else: # Unknown file format, just copy the stuff!
				image_type = None

			fp.close()

	except struct.error:
		raise IOError("Failed to decode header of image file")

	# Unknown image type
	if image_width is None or image_height is None:
		image_type = None

	return image_type, image_width, image_height

# TODO: Find a better implementation for this!
def htmlFormat(html, *args):
	def __escape__(r):
		if r.group(1) == "%":
			return "%"
		try:
			return cgi.escape(("%" + r.group(1)) % args.pop(0), quote=True)
		except IndexError:
			raise IndexError("htmlFormat did not get enough arguments to format the string")

	args = list(args)

	# From https://github.com/transifex/transifex/blob/devel/transifex/resources/formats/validators.py#L232
	html = re.sub(	'%((?:(?P<ord>\d+)\$|\((?P<key>\w+)\))?(?P<fullvar>[+#-]*(?:\d+)?'\
					'(?:\.\d+)?(hh\|h\|l\|ll)?(?P<type>[\w%])))', __escape__, html)
	if len(args) != 0:
		raise IndexError("htmlFormat did get too much arguments")
	return html

# Escape a single text
def htmlEscape(text):
	return cgi.escape(text, quote=True)

# Undo exactly what is done in: http://hg.python.org/cpython/file/2.7/Lib/cgi.py
def htmlUnescape(text):
	text = text.replace("&quot;", 	'"')
	text = text.replace("&gt;",		">")
	text = text.replace("&lt;",		"<")
	text = text.replace("&amp;",	"&")
	return text

def CDATAEscape(text):
	elements 	= []

	lastTextPos = 0
	for r in re.finditer("(]])>", text, re.DOTALL):
		textInbetween 	= text[ lastTextPos: r.start(1) ]
		lastTextPos 	= r.end(1)
		elements.append(textInbetween)

	textInbetween = text[ lastTextPos: ]
	if textInbetween != "":
		elements.append(textInbetween)

	return "<![CDATA[%s]]>" % ']]><![CDATA['.join(elements)

# Cleans up a given URL in order to find a normalzed version suitable as URL
def cleanURL(url, prefix="page"):

	# Convert url to lowercase
	url = url.lower().strip()

	# Several special formatting character issues with german text
	for search, replace in {'Ä': 'ae', 'Ö': 'oe', 'Ü': 'ue',
							'ä': 'ae', 'ö': 'oe', 'ü': 'ue',
							'ß': 'ss', '.': '-',  '@': '-at-' }.iteritems():
		url = url.replace(search, replace)

	# Filter all other characters (this version is better as it avoids multiple dashes behind each other)
	url = re.sub("[^a-zA-Z0-9-_/ \t\n\r\f\v]", "", url)
	url = re.sub("[-_/ \t\n\r\f\v]+", "-", url).strip('-')

	#url = re.sub("[ \t\n\r\f\v]+", "-", url)
	#url = re.sub("[^-a-zA-Z0-9]", "", url)

	# Add page in front of the url if no name was found or name too short
	if len(url) <= 4 and prefix is not None:
		if url != "":
			url = "-%s" % (url)
		url = "%s%s" % (prefix, url) # Append a prefix

	# Reduce URL length to max. 64 characters
	elif len(url) > 64:
		url = url[:64]

	return url

# Splits an iterable into several chunks of a given size
def chunkIter(iterable, size):
	it 		= iter(iterable)
	while True:
		chunk = list(itertools.islice(it, size))
		if not chunk:
			break
		yield chunk

# Convert a string to a boolean
def boolFromString(s, default=False):
	s = s.strip().lower()
	if s == 'y' or s == 'yes' or s == 'true' or s == '1':
		return True
	elif s == 'n' or s == 'no' or s == 'false' or s == '0':
		return False
	return default

# Convert a boolean to a string
def stringFromBool(b):
	if b:
		return 'yes'
	else:
		return 'no'

# Similar to os.path.join, but ensures that the subdir is really inside the given directory
# Please note that local mounts can still be a problem in some cases.
def securePathJoin(maindir, subdir):
	maindir 	= os.path.realpath(maindir)
	directory 	= os.path.realpath(os.path.join(maindir, subdir))
	maindir 	+= os.path.sep
	if directory[:len(maindir)] != maindir:
		raise RuntimeError("Possible exploit attempt! Path '%s' is outside of allowed directory '%s'." % (subdir, maindir))
	return directory

# Convert a timestamp to a datetime and set the correct timezone
def datetimeFromTimestamp(timestamp):
	return datetime.datetime.fromtimestamp(timestamp, tz=dateutil.tz.tzlocal())

# Current datetime object with timezone
def datetimeNow():
	return datetime.datetime.now(tz=dateutil.tz.tzlocal())

# Removes newlines from the text and replaces them with single spaces
def replaceNewlineStrip(text):
	if text is not None:
		text = re.sub("[\n\r\x00]+", " ", text).strip()
	return text

def encodeCMSComment(attributeDict):
	return "<!-- [CMS] %s -->\n" % \
		' '.join([htmlFormat("%s=\"%s\"", key, value) for key, value in attributeDict.iteritems()])

def decodeCMSComment(line):
	r = re.match("^<!-- \[CMS\](?P<arguments>(\\s+[a-zA-Z0-9]+=\"[^\"]*\")*)\\s+-->$", line.strip())
	if r is None:
		return None

	# Get arguments
	arguments 		= r.group('arguments')
	attributeDict 	= {}

	for r in re.finditer("(?P<key>[a-zA-Z0-9]+)=\"(?P<quotedValue>[^\"]*)\"", arguments, re.DOTALL):
		attributeDict[ r.group('key') ] = htmlUnescape(r.group('quotedValue'))

	# Return a dict of all attributes
	return attributeDict

# This object represents the resized version of an original image (.copy-image files)
class internalImage(object):
	""" This object represents a resized image based on an original version.
	"""

	def __init__(self, filename = None, original = None, *args, **kwds):
		""" Loads a image from a .copy-image file, or if no filename is given, a new file is created with
			the information about the original image.

			filename 		-- 	path to the .copy-image file
			original 		-- 	original image object
			width/height 	-- 	width and height of the resized image
			watermark 		-- 	should we add a watermark?

			possible exceptions:
				IOError 		-- Unable to read or write file
				RuntimeError 	-- Incorrect arguments

		"""

		self.filename 			= filename 			# Filename
		self.lastModified       = None 				# Last modified

		self.image_url 			= None 				# URL of this image
		self.image_width        = None
		self.image_height 		= None
		self.image_watermark    = False

		self.refOriginal		= None

		if filename is not None:
			self.reloadFile()

		elif original is not None:
			self.__createFile__(original, *args, **kwds)

		else:
			raise RuntimeError("No filename or original image object given")

	def reloadFile(self):
		global cms

		relImageFilename = None

		# Try to read the corresponding definition file
		with open(self.filename, 'r') as fp:

			while True:
				line = fp.readline()
				if line == "":
					break

				# In contrary to articles we parse the whole file!
				line = line.strip()

				# Split content of line and get key/value
				fields 	= line.split(":", 1)
				key 	= fields[0].strip().lower()
				value 	= fields[1].strip() if len(fields) >= 2 else ""

				if key == 'width':
					try:
						self.image_width = int(value)
					except ValueError:
						cms.error("Copy-image file '%s' contains a invalid value for key 'Width'", self.filename)

				elif key == 'height':
					try:
						self.image_height = int(value)
					except ValueError:
						cms.error("Copy-image file '%s' contains a invalid value for key 'Height'", self.filename)

				elif key == 'watermark':
					self.image_watermark = boolFromString(value)

				elif key == 'related-image':
					# The path given here is relative to the current file
					relImageFilename 	= os.path.join(os.path.dirname(self.filename), value)

				elif key == 'url':
					self.image_url = value

				elif len(key) and key[0] != '#':
					cms.warning("Copy-image file '%s' contains invalid key '%s'", self.filename, key)

			fp.close()

		# Append the .copy-image object to the original image
		if relImageFilename is None:
			cms.error("Copy-image file '%s' doesn't contain an 'Related-Image'-key and cannot be assigned to an image", self.filename)
			return

		# Tricky: The copy-image searches for the corresponding image and adds itself to the list of available versions :-)
		page = cms.getPageBySource(relImageFilename)
		if page is None:
			cms.error("Copy-image file '%s' references nonexistent source image '%s'", self.filename, relImageFilename)

		elif not isinstance(page, cmsImageOriginal):
			cms.error("Copy-image file '%s' references source '%s' which doesn't have the expected type image", self.filename, relImageFilename)

		else:
			self.refOriginal = page
			page.appendImage(self)

		# Use the cached URL
		if self.image_url:
			self.image_url = cms.uniqueURL(self.image_url, self)

	def __createFile__(self, original, width = None, height = None, watermark = False):
		global cms

		if original is None or not isinstance(original, cmsImageOriginal):
			raise RuntimeError("No valid original image given")

		elif original.getFilename() is None:
			raise RuntimeError("The original image should have a filename")

		originalImageFilename 	= original.getFilename()
		if not os.path.isfile(originalImageFilename):
			raise RuntimeError("The original image doesn't exist anymore")

		originalImageDirectory  = os.path.dirname(originalImageFilename)

		# This is required for generateURL()
		self.refOriginal = original

		# Remember settings :-) No validation required here as we dont write this directly to a file.
		self.image_width 		= width
		self.image_height 		= height
		self.image_watermark	= watermark

		# generate the new definition file
		dirname, basename 	= os.path.split(originalImageFilename)
		filename, ext 		= os.path.splitext(basename)

		if self.image_width is not None and self.image_height is not None:
			filename += "-%dx%d" % (self.image_width, self.image_height)

		if self.image_watermark:
			filename += "-watermark"

		# Generate filename starting with '.'
		filename 		= os.path.join(dirname, ".%s.copy-image" % filename)

		# Generate URL if does not exist
		if self.image_url is None:
			self.generateURL()

		# Create new unique file
		with cms.openUniqueFile(filename) as (self.filename, fp):
			if self.image_width is not None:
				fp.write( "Width:\t%d\n" % self.image_width )
			if self.image_height is not None:
				fp.write( "Height:\t%d\n" % self.image_height )
			fp.write( "Watermark:\t%s\n" % stringFromBool(self.image_watermark) )
			fp.write( "Related-Image:\t%s\n" % os.path.relpath(originalImageFilename, originalImageDirectory) )
			fp.write( "URL:\t%s\n" % self.image_url )
			fp.close()

		# Okay, everything good so far ... now safe the reference to the original object
		original.appendImage(self)

	def setLastModified(self, lastModified):
		self.lastModified = lastModified

	def getLastModified(self):
		return self.lastModified

	def getFilename(self):
		return self.filename

	def getWidth(self):
		self.__calculateSize__()
		return self.image_width

	def getHeight(self):
		self.__calculateSize__()
		return self.image_height

	def getWatermark(self):
		return self.image_watermark

	def __calculateSize__(self):

		# If the local image attributes are incomplete
		if 		self.image_width is None or \
				self.image_height is None:

			# Then check if the original image is associcated
			if self.refOriginal is None:
				raise RuntimeError("Unable to calculate image size! No reference to original image available.")

			# Get the original image size
			width, height = self.refOriginal.getImageSize()

			# Calculate missing width/height sizes
			if self.image_width is None and self.image_height is None:
				self.image_width 	= width
				self.image_height 	= height

			elif self.image_width is None:
				self.image_width = width * self.image_height / height

			elif self.image_height is None:
				self.image_height = height * self.image_width / width

	# Checks if a given internal image version matches the given keywords
	def matchesAttributes(self, **kwds):
		self.__calculateSize__()
		if kwds.has_key('watermark'):
			if self.image_watermark != kwds['watermark']:
				return False
		if kwds.has_key('width'):
			if self.image_width != kwds['width']:
				return False
		if kwds.has_key('height'):
			if self.image_height != kwds['height']:
				return False
		return True

	def getURL(self, absolute = True, websiteURL = None):
		global config

		if self.image_url is None:
			self.generateURL()

		if not absolute:
			return self.image_url

		# We use the STATIC_URL here because images should not be rendered dynamically
		return urlparse.urljoin(config.CMSGEN_STATIC_URL, self.image_url)

	def generateURL(self):
		global cms

		if self.image_url is not None:
			return

		# No reference to original given, unable to create!
		if 		self.refOriginal is None or \
				self.refOriginal.getFilename() is None:
			raise RuntimeError("Unable to generate image URL! No reference to original image available.")

		# Calculate missing dimension info if not given
		self.__calculateSize__()

		# Split filename into basename and ext
		url, ext = os.path.splitext(os.path.basename( self.refOriginal.getFilename() ))
		if self.image_width is not None and self.image_height is not None:
			url += "-%dx%d" % (self.image_width, self.image_height)
		if self.image_watermark:
			url += "-watermark"
		url = "user-images/%s%s" % (cleanURL(url, "image"), ext)
		self.image_url = cms.uniqueURL(url, self)

	def updateImage(self):
		global cms, config

		# No reference to original given, unable to create!
		if 		self.refOriginal is None or \
				self.refOriginal.getFilename() is None:
			raise RuntimeError("Unable to update images! No reference to original image available.")

		# Dont use absolute URL here cause we want to append websiteRoot in front of it
		websiteRoot = config.CMSGEN_WEBSITE_ROOT
		url 		= self.getURL(absolute=False)

		if websiteRoot is None or url is None:
			raise RuntimeError("No website root directory set or unable to generate URL - aborting")

		# Check when the source file was modified the last time!
		originalImageFilename 	= self.refOriginal.getFilename()

		# Image has not changed in the meantime?
		if self.lastModified is not None and self.refOriginal.getLastModified() < self.lastModified:
			return True

		# It has changed, we have to render it again
		cms.debug("Image '%s' has changed, updating content of '%s'", originalImageFilename, url)

		outputFilename = securePathJoin(websiteRoot, url)

		# We have no clue what this file format means - just copy it
		if self.refOriginal.getImageType() is None:
			shutil.copyfile(originalImageFilename, outputFilename )

		# We know the type, but we should use PIL - watch out, this is terrible quality
		elif config.IMAGE_USE_PIL:
			import PIL.Image

			# Load the image using PIL
			try:
				im = PIL.Image.open(originalImageFilename)
			except IOError:
				raise

			# Resize the image if necessary
			self.__calculateSize__()
			width, height = im.size
			if width != self.image_width or height != self.image_height:
				im.thumbnail((self.image_width, self.image_height))

			# Directly save it :-)
			im.save( outputFilename )

			# Free memory for image
			del im

		# We know the type and can use the linux command line tools! :-)
		else:
			commandLine = ["convert"]
			commandLine.append(originalImageFilename) 	# Source path
			commandLine.append("-verbose") 	# Verbosity

			# Resize the image if necessary
			self.__calculateSize__()
			width, height = self.refOriginal.getImageSize()
			if width != self.image_width or height != self.image_height:
				commandLine.append("-resize")
				commandLine.append("%dx%d" % (self.image_width, self.image_height))

			# Strip original comments
			commandLine.append("-strip")

			# Append comment
			if config.IMAGE_COMMENT is not None:
				commandLine.append("-comment")
				commandLine.append(config.IMAGE_COMMENT)

			# Desination path
			commandLine.append( outputFilename )

			if subprocess.call(commandLine, shell=False) != 0:
				raise IOError("Unable to update image content of %s" % url)

			# Call optipng to reduce filesize! :-)
			if self.refOriginal.getImageType() == 'png':
				commandLine = ["optipng", "-force", "-preserve"]
				commandLine.append( outputFilename )

				if subprocess.call(commandLine, shell=False) != 0:
					cms.warning("Image '%s' cannot be compressed with optipng", originalImageFilename)

		return True

# CMS Object base class
class cmsObject(object):
	""" Minimal implementation of a CMS object, which can have any type.
	"""

	# Reload the original files
	def reloadFile(self):
		pass

	# Will be called if a previous version of this generated file is found somewhere in the websiteRoot directory
	def setLastDigest(self, lastDigest):
		pass

	def setLastModified(self, lastModified):
		pass

	def getLastDigest(self):
		return None

	def getLastModified(self):
		return None

	# Required for rendering (in most cases)
	def getFilename(self):
		return None

	def getURL(self, absolute = True, kwds = None, websiteURL = None):
		return None

	def generateURL(self):
		return None # Do nothing

	def updatePage(self, output = None, **kwds):
		return False # Do nothing

# Minimal implementation of a CMS template page
class cmsTemplatePage(cmsObject):
	""" Minimal implementation of a CMS template page. This already implements a method to update the page based on a configurable template.
		This implementation still lacks a generateURL and configurePage method.
	"""

	def __init__(self):
		super(cmsTemplatePage, self).__init__()

		self.filename 			= None							# Filename
		self.lastDigest         = None 							# Last digit

		self.page_doctype       = "<!DOCTYPE html>"				# Doctype, first line of the resulting document
		self.page_url           = None 							# URL of this page

	def setLastDigest(self, lastDigest):
		self.lastDigest = lastDigest

	def getLastDigest(self):
		return self.lastDigest

	def getFilename(self):
		return self.filename

	# Register this absolute URL
	# TODO: Implement unregistering URLs and reregistering if it is already set
	def setURL(self, url):
		if self.page_url is None:
			self.page_url = cms.uniqueURL(url, self)

	def getURL(self, absolute=True, kwds=None, websiteURL = None):
		global config

		if self.page_url is None:
			self.generateURL()

		if not absolute:
			return self.page_url

		if websiteURL is None:
			return urlparse.urljoin(config.CMSGEN_WEBSITE_URL, self.page_url)
		else:
			return urlparse.urljoin(websiteURL, self.page_url)

	def generateURL(self, websiteURL = None):
		raise NotImplementedError("No generateURL method implemented")

	def updatePage(self, output = None, **kwds):
		global config

		# Dont use absolute URL here cause we want to append websiteRoot in front of it
		websiteRoot = config.CMSGEN_WEBSITE_ROOT
		url 		= self.getURL(absolute=False)

		if websiteRoot is None or url is None:
			raise RuntimeError("No website root directory set or unable to generate URL - aborting")

		# Build this page using the template object and fetch the digest
		pageBuilder = self.configurePage(**kwds)
		digest      = pageBuilder.build(output)

		# If the user just wanted a preview, it was already generated and written to output
		if output is not None:
			return False
		
		# Page has not changed?
		if digest == self.getLastDigest():
			return False

		cms.debug("Template page '%s' has changed, updating content of '%s'", self.getFilename(), url)

		outputFilename 	= securePathJoin(websiteRoot, url)

		# Create the path if it doesnt exist yet
		# Note: we dont delete old directories... who cares?
		outputDirectory = os.path.dirname(outputFilename)
		if not os.path.isdir(outputDirectory):
			os.makedirs(outputDirectory)

		# Page has changed
		with open(outputFilename, 'w') as fp:
			fp.write("%s\n" % self.page_doctype)
			fp.write(encodeCMSComment({"HASH": digest.encode('hex')}))	# Encoded attributes!

			try:
				pageBuilder.build(fp)

			# In case of an error we truncate the file to trigger regeneration on the next iteration!
			except:
				fp.seek(0)
				fp.write("\n")
				fp.write("<!-- [CMS] -->\n")
				fp.truncate()
				raise

			fp.close()

		return True

	def configurePage(self, **kwds):
		raise NotImplementedError("No configurePage method implemented")

# Default image object
class cmsImageOriginal(cmsObject):
	""" Original image inside of an article
	"""

	def __init__(self, filename, readonly = True):
		super(cmsImageOriginal, self).__init__()

		self.filename 			= filename
		self.readonly           = readonly

		self.lastModified       = None 				# Last modified

		self.image_type 		= None
		self.image_width 		= None
		self.image_height 		= None

		self.refImages 			= set()

		# Read image infos - has been put in a separate file to keep the source simpler to read.
		try:
			self.image_type, self.image_width, self.image_height 	= getImageInfo(self.filename)
			self.lastModified 										= os.path.getmtime(self.filename)
		except (IOError, OSError):
			raise

	def getLastModified(self):
		return self.lastModified

	def getImageType(self):
		return self.image_type

	def getImageSize(self):
		return (self.image_width, self.image_height)

	# Requires to automatically assign copy-image files (identified by filename)
	def appendImage(self, image):
		self.refImages.add(image)

	def getFilename(self):
		return self.filename


	def getURL(self, absolute = True, kwds = None, websiteURL = None):
		global cms

		# Unable to determine exact type or SVG graphics - we only need one instance
		# in this case
		if self.image_type is None:

			# Note: As the browser has to resize this manually, we dont remove the width and height keywords if there are any!
			try:
				return iter(self.refImages).next().getURL(absolute=absolute, websiteURL=websiteURL)
			except StopIteration:
				pass

			# Otherwise we ignore the width and height and just request a new one
			width 		= None
			height 		= None
			watermark 	= False

		# Generate specific image
		else:

			# Okay, this type is well-known! First manually parse the width and height tags
			try:
				width = kwds['width'].strip()
				width = int( float(width[:-1]) * self.image_width / 100 ) 		if width.endswith('%') else int(width)
				del kwds['width']
			except (TypeError, KeyError, ValueError):
				width = None

			try:
				height = kwds['height'].strip()
				height = int( float(height[:-1]) * self.image_height / 100 ) 	if height.endswith('%') else int(height)
				del kwds['height']
			except (TypeError, KeyError, ValueError):
				height = None

			# Calculate missing width / height attributes if not everything is given
			if width is None and height is None:
				width 	= self.image_width
				height 	= self.image_height

			elif width is None:
				width 	= self.image_width * height / self.image_height

			elif height is None:
				height 	= self.image_height * width / self.image_width

			# Replace input fields, as we have already handled percentage values
			if kwds is not None:
				kwds['image-width'] 	= str(width) 		# Write out the final sizes
				kwds['image-height']	= str(height) 		# Write out the final sizes

			# Does the user want a watermark?
			watermark = kwds.has_key('watermark') if kwds is not None else False

			# We are forced to create a new image?!
			for image in self.refImages:
				if image.matchesAttributes(width=width, height=height, watermark=watermark):
					return image.getURL(absolute=absolute, websiteURL=websiteURL)

		# We shouldn't create new images in readonly mode
		if self.readonly:
			return None

		# Create a new modified image
		try:
			image = internalImage(None, self, width, height, watermark)
		except IOError:
			cms.error("Unable to create preview image for '%s'", filename)
			return None

		# Return newly generated URL
		return image.getURL(absolute, websiteURL=websiteURL)

	def generateURL(self):
		for image in self.refImages:
			image.generateURL()

	def updatePage(self, output = None, **kwds):
		
		# Output to something else?
		if output is not None:
			return False

		# Update iamges if anything has changed
		for image in self.refImages:
			image.updateImage()

		return True # Do nothing

# Template HTML pages
class cmsHTMLPage(cmsTemplatePage):
	""" Represents any kind of HTML template page
	"""

	def __init__(self):
		super(cmsHTMLPage, self).__init__()
		global config

		self.page_menubartitle	= None 							# menubarTitle
		self.page_menubarindex  = None 							# menubarIndex

		self.page_title 		= "" 							# Title of this content page
		self.page_author 		= config.CMSGEN_DEFAULT_AUTHOR 	# Author
		self.page_tags   		= set() 						# Tags
		self.page_datetime      = None 							# Datetime

		self.page_showinindex   = False 						# If this page should show up in the index

	# (optional)
	def setMenubarTitle(self, menubarTitle):
		self.page_menubartitle = menubarTitle

	# (optional)
	def setMenubarIndex(self, menubarIndex):
		self.page_menubarindex = menubarIndex

	# Required for menubar
	def getMenubarTitle(self):
		return self.page_menubartitle

	# Required for menubar
	def getMenubarIndex(self):
		return self.page_menubarindex

	# For template generation
	def getTitle(self):
		return self.page_title

	# For template generation
	def getAuthor(self):
		return self.page_author

	# For template generation
	def getTags(self):
		return self.page_tags

	# For template generation
	def getDateTime(self):
		return self.page_datetime

	# For generateIndexPage
	def getShowInIndex(self):
		return self.page_showinindex

	# Render using the HTMLPage template script
	def configurePage(self, **kwds):
		return cmsTemplate.configureHTMLPage(self, **kwds)

# Template RSS Feed page
class cmsRSSFeed(cmsTemplatePage):
	""" Represents a RSS feed page
	"""
	def __init__(self):
		super(cmsRSSFeed, self).__init__()
		self.page_doctype 		= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

	def configurePage(self, **kwds):
		return cmsTemplate.configureRSSFeed(self, **kwds)

# Sitemap
class cmsSitemap(cmsTemplatePage):

	def __init__(self, refArticles):
		super(cmsSitemap, self).__init__()

		self.page_doctype = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

		self.refArticles  = refArticles

	def generateURL(self):
		global cms

		if self.page_url is None:
			url = "sitemap"
			url = "%s.xml" % cleanURL(url)
			self.page_url = cms.uniqueURL(url, self)

	def configurePage(self, **kwds):
		return cmsTemplate.configureSitemap(self, **kwds)

	# Return a list of articles included in this list
	def getRefArticles(self):
		return self.refArticles

	def updatePage(self, output = None, **kwds):
		if super(cmsSitemap, self).updatePage(output, **kwds):
			
			websiteRoot = config.CMSGEN_WEBSITE_ROOT
			url 		= self.getURL(absolute=False)

			if websiteRoot is None or url is None:
				raise RuntimeError("No website root directory set or unable to generate URL - aborting")

			gzURL = "%s.gz" % url

			outputFilename 		= securePathJoin(websiteRoot, url)
			gzOutputFilename	= securePathJoin(websiteRoot, gzURL)

			# Which version should we send to crawlers?
			useGz = False

			# Generate .tar.gz version
			if os.path.isfile(outputFilename):
				commandLine = ["gzip"]
				commandLine.append("--to-stdout") # To stdout instead of overwriting anything
				commandLine.append(outputFilename)

				with open(gzOutputFilename, 'wb') as fp:
					result = subprocess.call(commandLine, shell=False, stdout=fp)

				# Print message if something went wrong!
				if result != 0:
					# Delete corrupted .gz file
					os.unlink(gzOutputFilename)
					cms.warning("Sitemap '%s' cannot be compressed with gzip!", outputFilename)

				else:
					useGz = True

			# We prefer to invalidate the gz compressed sitemap
			if useGz:
				cms.addInvalidateSitemapURL(gzURL)
			else:
				cms.addInvalidateSitemapURL(url)

# Simple static page without any comments (for example impressum)
class cmsArticle(cmsHTMLPage):
	""" Article page based on a file containing headers
	"""

	def __init__(self, filename, readonly = True):
		super(cmsArticle, self).__init__()

		self.lock_generate		= threading.RLock()

		self.filename 			= filename
		self.readonly           = readonly

		self.page_image 		= None 						# Image for this article
		self.page_group			= None						# Name of the group
		self.page_showinindex   = True 						# By default all articles are included in the inde

		self.reloadFile()

	def reloadFile(self):
		global cms, config

		# Reset in case we are reloading
		self.page_datetime = None

		# We only read the header, not the content following afterwards
		with open(self.filename, 'r' if self.readonly else 'r+') as fp:

			lastHeaderPos = None

			while True:
				lastHeaderPos 	= fp.tell()
				line 			= fp.readline().strip()
				if line == "":
					break

				# Split content of line and get key/value
				fields 	= line.split(":", 1)
				key 	= fields[0].strip().lower()
				value 	= fields[1].strip() if len(fields) >= 2 else ""

				if key == 'title':
					self.page_title = value

				elif key == 'author':
					self.page_author = value

				elif key == 'tags':
					for tag in value.split(','):
						tag = tag.strip()
						if tag != "": # Ignore empty tags
							self.page_tags.add( tag )

				elif key == 'image':
					self.page_image = value

				elif key == 'group':
					self.page_group = value.lower()			

				elif key == 'menubar-title':
					self.page_menubartitle = value

				elif key == 'menubar-index':
					try:
						self.page_menubarindex = int(value)
					except ValueError:
						cms.error("Article '%s' contains a invalid value for key 'Menubar-Index'", self.filename)

				elif key == 'datetime':
					try:
						self.page_datetime = dateutil.parser.parse(value)
					except ValueError:
						cms.error("Article '%s' contains a invalid value for key 'DateTime'", self.filename)

				elif key == 'show-in-index':
					self.page_showinindex 	= boolFromString(value)

				elif key == 'url':
					self.page_url = value

				elif len(key) and key[0] != '#':
					cms.warning("Article '%s' contains invalid header key '%s'", self.filename, key)

			# In some cases some changes might be necessary...
			changes = []

			# Remaining informations
			if self.page_datetime is None:
				self.page_datetime 	= datetimeFromTimestamp( os.path.getmtime(self.filename) )

				if not self.readonly and self.page_showinindex:
					changes.append("DateTime:\t%s" % self.page_datetime.strftime(config.CMSGEN_TIME_FORMAT))

			# If this object already has an URL assigned?
			if self.page_url is not None:
				if cms.getPageByURL(self.page_url) != self: # not our page?!
					self.page_url = cms.uniqueURL(self.page_url, self)

			# For pages shown in the index we assign a fixed URL!
			elif self.page_url is None and not self.readonly and (self.page_showinindex or self.page_group):

				# Generate a new unique URL and remember it for later usage
				self.generateURL()
				changes.append("URL:\t%s" % self.page_url)

			# if an update is required
			if not self.readonly and len(changes):
				cms.warning("Article '%s' will be modified automatically", self.filename)

				# TODO: This would be a good place to create some kind of backup in case
				# something does wrong

				# TODO: What should we do if the article is too big for our memory?
				content = fp.read()

				fp.seek(lastHeaderPos)
				for change in changes:
					fp.write("%s\n" % change)
				fp.write("\n")

				fp.write(content)
				fp.truncate()

			# Set readonly to true to prevent modifying the files in a second iteration!
			self.readonly = True

			# Finally close the file
			fp.close()

	def generateURL(self):
		global cms

		if self.page_url is None:

			# Generate a URL!
			if self.page_menubartitle is not None:
				url = self.page_menubartitle
			elif self.page_title != "":
				url = self.page_title
			elif self.filename is not None:
				url = os.path.splitext(os.path.basename(self.filename))[0]
			else:
				url = "noname"

			# Append .html to the cleaned up url
			url = "%s.html" % cleanURL(url)
			if self.page_showinindex:
				url = "articles/%s/%s" % (self.page_datetime.strftime("%Y-%m"), url)
			elif self.page_group:
				url = "%s/%s" % (self.page_group, url)
			self.page_url = cms.uniqueURL(url, self)

	# Return article image
	def getImage(self):
		return self.page_image

# List of pages for example for index or tag pages
class cmsListOfPages(cmsHTMLPage):
	""" A list of pages includes the preview text for a list of regular articles
	"""

	def __init__(self, refArticles, tagOrName = None, pageIndex = 0, numberOfPages = 1):
		super(cmsListOfPages, self).__init__()

		self.refArticles 		= refArticles 		# Articles included in this list

		self.tagOrName 			= tagOrName 		# Tag or name of the list
		self.pageIndex      	= pageIndex 		# Index of this page
		self.numberOfPages  	= numberOfPages 	# Total number of pages

		self.refPrevPage    	= None 				# References to the previous and next pages
		self.refNextPage    	= None 				# (if any)

		# Set title if tagOrName is given :)
		if tagOrName is not None:
			self.page_title 	= "Tag %s" % tagOrName
			self.page_filter	= "Articles for the tag: %s" % tagOrName

		else:
			self.page_title 	= "News"
			self.page_filter 	= ""

	def generateURL(self):
		global cms

		if self.page_url is None:

			# Convert url to lowercase
			if self.tagOrName is not None:
				url = self.tagOrName.lower().strip()
			else:
				url = "index"

			if self.pageIndex != 0:
				url += "-%d" % self.pageIndex

			# Append .html to the cleaned up url
			url = "%s.html" % cleanURL(url, prefix="tag")
			if self.tagOrName is not None:
				url = "tags/%s" % url
			self.page_url = cms.uniqueURL(url, self)

	def getFilter(self):
		return self.page_filter

	# Set reference to prev page
	def setRefPrevPage(self, refPrevPage):
		self.refPrevPage = refPrevPage

	# Set reference to next page
	def setRefNextPage(self, refNextPage):
		self.refNextPage = refNextPage

	# Return a list of articles included in this list
	def getRefArticles(self):
		return self.refArticles

	# Return a reference to the previous page
	def getRefPrevPage(self):
		return self.refPrevPage

	# Return a reference to the next page
	def getRefNextPage(self):
		return self.refNextPage

	# Return the page index
	def getPageIndex(self):
		return self.pageIndex

	# Return the total number of pages
	def getNumberOfPages(self):
		return self.numberOfPages

# Tag cloud
class cmsTagCloud(cmsHTMLPage):

	def __init__(self):
		super(cmsTagCloud, self).__init__()

		# Assign page title
		self.page_title = "Tag cloud"


	def generateURL(self):
		global cms

		if self.page_url is None:
			url = "tagcloud"
			url = "%s.html" % cleanURL(url)
			self.page_url = cms.uniqueURL(url, self)

# Sitemap
class cmsRSSListOfPages(cmsRSSFeed):

	def __init__(self, refArticles, pageIndex = 0, numberOfPages = 1):
		super(cmsRSSListOfPages, self).__init__()

		self.refArticles 		= refArticles 		# Articles included in this list

		self.pageIndex      	= pageIndex 		# Index of this page
		self.numberOfPages  	= numberOfPages 	# Total number of pages

		self.refPrevPage    	= None 				# References to the previous and next pages
		self.refNextPage    	= None 				# (if any)

	def generateURL(self):
		global cms

		if self.page_url is None:
			url = "index"
			if self.pageIndex != 0:
				url += "-%d" % self.pageIndex
			url = "feed/%s.xml" % cleanURL(url)
			self.page_url = cms.uniqueURL(url, self)

	# Set reference to prev page
	def setRefPrevPage(self, refPrevPage):
		self.refPrevPage = refPrevPage

	# Set reference to next page
	def setRefNextPage(self, refNextPage):
		self.refNextPage = refNextPage

	# Return a list of articles included in this list
	def getRefArticles(self):
		return self.refArticles

	# Return a reference to the previous page
	def getRefPrevPage(self):
		return self.refPrevPage

	# Return a reference to the next page
	def getRefNextPage(self):
		return self.refNextPage

	# Return the page index
	def getPageIndex(self):
		return self.pageIndex

	# Return the total number of pages
	def getNumberOfPages(self):
		return self.numberOfPages


# Template generator
class cmsTemplate(object):

	def __init__(self):
		raise NotImplementedError("You cannot create an instance of cmsTemplate")

	@staticmethod
	def __menubar__(currentPage = None, websiteURL = None):
		""" Generates the html code for the menubar.
			Optionally the menubar can depend on the current opened page.
		"""

		# All positve indices to the beginning, all negative indices to the end
		# For elements with the same index sort by menubar-title
		def __sortByIndex__(a, b):
			if a.getMenubarIndex() is None:
				group_a = 0
			elif a.getMenubarIndex() >= 0:
				group_a = -1
			else:
				group_a = 1

			if b.getMenubarIndex() is None:
				group_b = 0
			elif b.getMenubarIndex() >= 0:
				group_b = -1
			else:
				group_b = 1

			if group_a != group_b:
				return group_a - group_b

			if group_a != 0 and a.getMenubarIndex() != b.getMenubarIndex():
				return a.getMenubarIndex() - b.getMenubarIndex()

			return cmp(a.getMenubarTitle(), b.getMenubarTitle())

		global cms

		# Get all menubar pages
		menubarPages = []
		
		for page in cms.getPages():
			if not isinstance(page, cmsHTMLPage):
				continue

			elif page.getMenubarTitle() is not None:
				menubarPages.append(page)

		menubarPages.sort(cmp=__sortByIndex__)

		# Generate html output
		html = ""
		for page in menubarPages:
			html += htmlFormat(	"<a class=\"link\" href=\"%s\"><span class=\"entry\">%s</span></a>", \
								page.getURL(websiteURL=websiteURL), page.getMenubarTitle() )

		# The actual rendering is only done when needed!
		return buildRawData(html)

	@staticmethod
	def __tagCloud__(websiteURL = None):
		""" Generates the html code for the tagcloud...
		"""

		global cms

		# Get all tags ...
		all_tags 	= [] # count, tag, page

		for tag, tagPage in cms.pageByTag.iteritems():
			count = 0

			temp = tagPage
			while temp:
				count += len(temp.getRefArticles())
				temp = temp.getRefNextPage()

			all_tags.append((count, tag, tagPage))

		# Sort by number of occurances
		all_tags.sort(reverse=True)
		#all_tags.sort(key = lambda x: x[0], reverse=True)

		if len(all_tags) == 0:
			return buildRawData("No tags added yet!")

		# Only use top values for tagCloud
		tags 		= all_tags[:100]
		max_count 	= tags[0][0]
		min_count 	= tags[-1][0]

		# Sort by tag name converted to lowercase
		tags.sort(key = lambda x: x[1].lower())

		html = "<div class=\"tagcloud\">"

		for count, tag, page in tags:
			url 	= page.getURL(websiteURL=websiteURL) if page is not None else "#"

			# Map to a corresponding font size
			fontsize = (1.0 + 2.0 * float(count - min_count) / (max_count - min_count)) if max_count != min_count else 1.0
			html += htmlFormat("<a class=\"link\" href=\"%s\"><span class=\"tag\" style=\"font-size:%.2fem;\">%s</span></a>\n", url, fontsize, tag) 

		html += "</div>"
		return buildRawData(html)

	@staticmethod
	def __includes__(websiteURL = None):
		global cms, config

		stylesheetList = ["css/style.css", "css/ismobile.css"]
		javascriptList = []

		stylesheetList.append("css/highlight.css")
		stylesheetList.append("css/lightbox.css")
		javascriptList.append("js/jquery-1.8.3.min.js")
		javascriptList.append("js/lightbox.js")
		# javascriptList.append("share/jquery.socialshareprivacy.min.js")
		javascriptList.append("mathjax/MathJax.js?config=minimal")

		# Add stylesheets and javascript includes ...
		htmlIncludes = ""
		
		for stylesheet in stylesheetList:
			htmlIncludes += htmlFormat("<link rel=\"stylesheet\" type=\"text/css\" href=\"%s\" />\n", 	urlparse.urljoin(config.CMSGEN_STATIC_URL, stylesheet))
		
		for javascript in javascriptList:
			htmlIncludes += htmlFormat("<script type=\"text/javascript\" src=\"%s\"></script>\n", 		urlparse.urljoin(config.CMSGEN_STATIC_URL, javascript))

		# Social share button stuff
		# htmlIncludes += """<script type=\"text/javascript\">
		# 	$.fn.socialSharePrivacy.settings.order = ['facebook', 'gplus', 'twitter', 'hackernews'];
		# 	$(document).ready(function () {
		# 		$('.share').socialSharePrivacy();
		# 	});		
		# </script>\n"""

		if websiteURL is None:
			feedURL = urlparse.urljoin(config.RSS_URL_PREFIX, urlparse.urljoin(config.CMSGEN_WEBSITE_URL, "feed/"))
		else:
			feedURL = urlparse.urljoin(config.RSS_URL_PREFIX, urlparse.urljoin(websiteURL, "feed/"))

		# Feed URL
		htmlIncludes += htmlFormat("<link rel=\"alternate\" type=\"application/rss+xml\" title=\"%s\" href=\"%s\" />\n",
				config.RSS_FEED_NAME, feedURL )

		# Favicon
		if config.CMSGEN_FAVICON_TYPE and config.CMSGEN_FAVICON_URL:
			htmlIncludes += htmlFormat("<link rel=\"icon\" type=\"%s\" href=\"%s\" />",
					config.CMSGEN_FAVICON_TYPE, urlparse.urljoin(config.CMSGEN_STATIC_URL, config.CMSGEN_FAVICON_URL ) )

		return buildRawData(htmlIncludes)

	@staticmethod
	def __blogfooter__(websiteURL = None):
		global cms, config

		htmlFooter		= ""

		# Link to DPCMS
		page = cms.getPageByTag("DP-CMS")
		if page is not None:
			htmlFooter 		+= htmlFormat("<a class=\"linktag\" href=\"%s\"><span class=\"blogsystem\">DP-CMS %s</span></a>\n", page.getURL(websiteURL=websiteURL), config.CMSGEN_VERSION)

		# Link to Impressum
		page = cms.getPageBySource("source/impressum.txt")
		if page is not None:
			htmlFooter		+= htmlFormat("<a class=\"linktag\" href=\"%s\"><span class=\"impressum\">Contact / Impressum</span></a>\n", page.getURL(websiteURL=websiteURL))

		if websiteURL is None:
			feedURL = urlparse.urljoin(config.RSS_URL_PREFIX, urlparse.urljoin(config.CMSGEN_WEBSITE_URL, "feed/"))
		else:
			feedURL = urlparse.urljoin(config.RSS_URL_PREFIX, urlparse.urljoin(websiteURL, "feed/"))

		# Link to RSS feed
		htmlFooter		+= htmlFormat("<a class=\"linktag\" href=\"%s\" target=\"_blank\"><span class=\"feed\"><img class=\"icon\" src=\"%s\" alt=\"RSS\" />RSS - Feed</span></a>", feedURL, urlparse.urljoin(config.CMSGEN_STATIC_URL, "images/feed.png"))

		return buildRawData(htmlFooter)

	@classmethod
	def configureHTMLPage(cls, currentPage, websiteURL = None, **layoutKwds):
		""" Generates the HTML output for any page (derived from base class cmsHTMLPage).
		"""
		global cms, config

		if not isinstance(currentPage, cmsHTMLPage):
			raise TypeError("The page you specified is not derived from cmsHTMLPage")

		template = buildTemplate(buildFile("templates/template-static.html"))
		template.assignElement("menubar", 		cls.__menubar__(currentPage, websiteURL=websiteURL))
		template.assignElement("title", 		buildRawData(currentPage.getTitle(), formatFn=htmlEscape))
		template.assignElement("includes", 		cls.__includes__(websiteURL=websiteURL))
		template.assignElement("blogfooter", 	cls.__blogfooter__(websiteURL=websiteURL))

		# Is it just a static page
		if isinstance(currentPage, cmsArticle):

			htmlMeta = ""
			htmlMeta += htmlFormat("<meta name=\"keywords\" 		content=\"%s\" />\n", ', '.join(currentPage.getTags()))
			htmlMeta += htmlFormat("<meta name=\"author\"			content=\"%s\" />\n", currentPage.getAuthor())
			template.assignElement("metatags", buildRawData(htmlMeta))

			template.assignElement("filter", 		buildRawData(""))
			template.assignElement("articles", 		cls.configureArticle(currentPage, websiteURL=websiteURL, **layoutKwds))
			template.assignElement("prevnextpage", 	buildRawData(""))

		# List of pages (tag / index page)
		elif isinstance(currentPage, cmsListOfPages):

			htmlMeta = ""
			htmlMeta += htmlFormat("<meta name=\"author\"			content=\"%s\" />\n", currentPage.getAuthor())
			template.assignElement("metatags",		buildRawData(htmlMeta))

			# Header
			htmlFilter = htmlFormat("<div class=\"header\">%s</div>", currentPage.getFilter()) if currentPage.getFilter() else ""
			template.assignElement("filter",  		buildRawData(htmlFilter) )

			# List of Articles
			articleList = buildList()
			for article in currentPage.getRefArticles():
				articleList.appendElement( cls.configureArticle(article, preview=True, websiteURL=websiteURL, **layoutKwds) )
			template.assignElement("articles", articleList)

			# Current page and links for next / previous page
			htmlMorePosts = "<div class=\"moreposts\">"

			if currentPage.getRefNextPage() is not None:
				htmlMorePosts += htmlFormat("<span class=\"older\"><a class=\"link\" href=\"%s\">« older posts</a></span>", currentPage.getRefNextPage().getURL(websiteURL=websiteURL))
			if currentPage.getRefPrevPage() is not None:
				htmlMorePosts += htmlFormat("<span class=\"newer\"><a class=\"link\" href=\"%s\">newer posts »</a></span>", currentPage.getRefPrevPage().getURL(websiteURL=websiteURL))

			htmlMorePosts += htmlFormat("<span class=\"siteindex\">Site %d of %d</span>", currentPage.getPageIndex() + 1, currentPage.getNumberOfPages())
			htmlMorePosts += "</div>"
			template.assignElement("prevnextpage", buildRawData(htmlMorePosts))
		
		# Tag cloud
		elif isinstance(currentPage, cmsTagCloud):

			htmlMeta = ""
			htmlMeta += htmlFormat("<meta name=\"author\"			content=\"%s\" />\n", currentPage.getAuthor())
			template.assignElement("metatags",		buildRawData(htmlMeta))

			template.assignElement("filter",       	buildRawData(""))
			template.assignElement("articles", 		cls.__tagCloud__(websiteURL=websiteURL)) #cls.configureArticle(currentPage))
			template.assignElement("prevnextpage", 	buildRawData(""))

		else:
			raise NotImplementedError("This template doesnt support the page type you specified")

		return template

	@classmethod
	def configureArticle(cls, article, preview = False, websiteURL = None, **layoutKwds):
		""" Generates the HTML code for an article containing the content and some comments 
		"""
		global cms, config

		def __formatTags__(tags):
			html = ""

			if len(tags) > 0:
				html += "<div class=\"tags\"><span class=\"tagspan\">Tags:</span>"
				for tag in tags:

					# Try to get URL - this fails in case we skipped the step for generating tag pages,
					# if this is the case just link to #
					page 	= cms.getPageByTag(tag)
					url 	= page.getURL(websiteURL=websiteURL) if page is not None else "#"
					html 	+= htmlFormat("<a class=\"linktag\" href=\"%s\"><span class=\"tag\">%s</span></a>", url, tag)

				html += "</div>"

			return html

		def __lookupInternalURL__(filename, kwds=None):

			articleFilename = article.getFilename()
			if articleFilename is None:
				cms.error("Tag [aimg] and [aurl] only possible if article has a filename")
				raise KeyError("No article filename set")

			relFilename 	= os.path.join(os.path.dirname(articleFilename), filename)

			page = cms.getPageBySource( relFilename )
			if page is None:
				cms.error("File or image %s not found", filename)
				raise KeyError("File or image not found")

			# TODO: Append correct arguments for preview pages
			url = page.getURL(kwds=kwds, websiteURL=websiteURL)
			if url is None:
				cms.error("Unable to generate URL for file/image %s", filename)
				raise KeyError("Unable to generate URL")

			return url

		def __lookupTagURL__(tag):

			page = cms.getPageByTag(tag)
			if page is None:
				cms.error("Tag %s not found", tag)
				raise KeyError("Tag not found")

			url = page.getURL()
			if url is None:
				cms.error("Unable to generate URL for tag page")
				raise KeyError("Unable to generate URL")

			return url

		def __formatBBcode__(content):
			return pureBBcode.htmlBBcode(	content, \
											previewMode 		= preview, \
											staticURL 			= config.CMSGEN_STATIC_URL, \
											websiteURL 			= config.CMSGEN_WEBSITE_URL if websiteURL is None else websiteURL, \
											lookupInternalURL 	= __lookupInternalURL__, \
											lookupTagURL 		= __lookupTagURL__, \
											internalHostnames 	= config.BBCODE_INTERNAL_HOSTNAMES
										)

		if not isinstance(article, cmsArticle):
			raise TypeError("The page you specified is not derived from cmsArticle")

		template = buildTemplate(buildFile("templates/template-article.html"))

		# Title and readmore buttons
		if preview:
			htmlTitle 		= htmlFormat("<a class=\"link\" href=\"%s\">%s</a>", article.getURL(websiteURL=websiteURL), article.getTitle())
			htmlReadmore 	= htmlFormat("<div class=\"readmore\"><a class=\"link\" href=\"%s\">« Read more »</a></div>", article.getURL(websiteURL=websiteURL))
		else:
			htmlTitle 		= htmlEscape(article.getTitle())
			htmlReadmore 	= ""

		template.assignElement("title", 	buildRawData(htmlTitle))
		template.assignElement("readmore",	buildRawData(htmlReadmore))
		template.assignElement("datetime",	buildRawData(article.getDateTime().strftime("%d %b %Y %H:%M %Z"), formatFn=htmlEscape))
		template.assignElement("author",	buildRawData(article.getAuthor(), formatFn=htmlEscape))

		# Resolve the image
		if article.getImage() is not None:

			try:
				kwds = {'width' : '90'}	# We force the width to 90 px

				# Get URL
				url = __lookupInternalURL__(article.getImage(), kwds)
				url = urlparse.urljoin( config.CMSGEN_STATIC_URL, url )

				htmlSize = ""
				if kwds.has_key('width') and kwds['width'] is not None:
					htmlSize += " width=\"%s\"" % htmlEscape(kwds['width'])
				if kwds.has_key('height') and kwds['height'] is not None:
					htmlSize += " height=\"%s\"" % htmlEscape(kwds['height'])

				urlEscaped 	= htmlEscape(url)
				htmlImage 	= "<img class=\"image\" src=\"%s\" alt=\"[article-image]\"%s />" % (urlEscaped, htmlSize)

				# In preview mode, make the image clickable! :)
				if preview:
					htmlImage = "<a class=\"link\" href=\"%s\">%s</a>" % (htmlEscape(article.getURL(websiteURL=websiteURL)), htmlImage)

			except KeyError:
				htmlImage = ""

		else:
			htmlImage = ""

		template.assignElement("image",		buildRawData(htmlImage))

		# Content and tags
		template.assignElement("content", 	buildFile(article.getFilename(), skipHeader=True, formatFn=__formatBBcode__))
		template.assignElement("tags",      buildRawData( sorted(article.getTags()), formatFn=__formatTags__))
		template.assignElement("share",		buildRawData("<div class=\"share\"></div>" if not preview and article.getShowInIndex() else ""))

		return template

	@classmethod
	def configureSitemap(cls, currentPage, websiteURL = None, **layoutKwds):
		""" Generates the sitemap stuff
		"""
		global cms, config

		# All positve indices to the beginning, all negative indices to the end
		# For elements with the same index sort by menubar-title
		def __sortByModificationTime__(a, b):
			if a[0] != b[0]:
				return cmp(b[0], a[0])

			return cmp(a[1].getTitle(), b[1].getTitle())

		def __entry__(modificationTime, page = None):

			if page is not None:
				url 		= urlparse.urljoin(config.SITEMAP_URL_PREFIX, page.getURL(websiteURL=websiteURL))
				changeFreq  = "weekly"
				priority    = "0.6"

			else:
				url 		= urlparse.urljoin(config.SITEMAP_URL_PREFIX, config.CMSGEN_WEBSITE_URL if websiteURL is None else websiteURL)
				changeFreq  = "daily"
				priority    = "1.0"

			return htmlFormat( "<url>\n\t<loc>%s</loc>\n\t<lastmod>%s</lastmod>\n\t<changefreq>%s</changefreq>\n\t<priority>%s</priority>\n\t</url>\n",
				url, modificationTime.replace(microsecond=0).isoformat(), changeFreq, priority )

		if not isinstance(currentPage, cmsSitemap):
			raise TypeError("The page you specified is not derived from cmsSitemap")

		# Get the modification times
		sitemapPages = []

		for page in currentPage.getRefArticles():
			modificationTime = datetimeFromTimestamp(os.path.getmtime( page.getFilename() )) if page.getFilename() is not None else page.getDateTime()
			sitemapPages.append((modificationTime, page))

		# Sort the entries by time
		sitemapPages.sort(cmp=__sortByModificationTime__)

		html = "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n"

		if len(sitemapPages) > 0:
			html += __entry__(sitemapPages[0][0], None)

		# Add other pages
		for (modificationTime, page) in sitemapPages:
			html += __entry__(modificationTime, page)

		html += "</urlset>"

		return buildRawData(html)

	@classmethod
	def configureRSSFeed(cls, currentPage, websiteURL = None, **layoutKwds):
		""" Configures a RSS Feed page
		"""
		global cms, config

		if not isinstance(currentPage, cmsRSSFeed):
			raise TypeError("The page you specified is not derived from cmsRSSFeed")

		template = buildTemplate(buildFile("templates/template-rss-static.xml"))
		template.assignElement("url", 			buildRawData(config.RSS_URL_PREFIX, formatFn=htmlEscape) )


		if isinstance(currentPage, cmsRSSListOfPages):

			#lastModified = datetimeNow()
			lastModified = cms.getLastModified()
			if lastModified is not None:
				template.assignElement("lastModified", 	buildRawData(lastModified.strftime("%a, %d %b %Y %H:%M:%S %Z"), formatFn=htmlEscape))
			else:
				template.assignElement("lastModified",	buildRawData(""))

			# Current page and links for next / previous page
			# See: http://www.ietf.org/rfc/rfc5005.txt
			xmlMorePosts = 	htmlFormat("\t\t<atom:link rel=\"self\" href=\"%s\" />\n", urlparse.urljoin(config.RSS_URL_PREFIX, currentPage.getURL(websiteURL=websiteURL)))
			if currentPage.getRefPrevPage() is not None:
				xmlMorePosts += htmlFormat("\t\t<atom:link rel=\"previous\" href=\"%s\" />\n", urlparse.urljoin(config.RSS_URL_PREFIX, currentPage.getRefPrevPage().getURL(websiteURL=websiteURL)))
			if currentPage.getRefNextPage() is not None:
				xmlMorePosts += htmlFormat("\t\t<atom:link rel=\"next\" href=\"%s\" />\n", urlparse.urljoin(config.RSS_URL_PREFIX, currentPage.getRefNextPage().getURL(websiteURL=websiteURL)))
			template.assignElement("prevnextpage", 	buildRawData(xmlMorePosts))

			# List of articles
			articleList = buildList()
			for article in currentPage.getRefArticles():
				articleList.appendElement( cls.configureRSSArticle(article, websiteURL=websiteURL, **layoutKwds) )
			template.assignElement("items", articleList)

		else:
			raise NotImplementedError("This template doesnt support the page type you specified")

		return template

	@classmethod
	def configureRSSArticle(cls, article, websiteURL = None, **layoutKwds):
		""" Generates the HTML code for an article containing the content
		"""
		global cms, config

		def __lookupInternalURL__(filename, kwds=None):

			articleFilename = article.getFilename()
			if articleFilename is None:
				cms.error("Tag [aimg] and [aurl] only possible if article has a filename")
				raise KeyError("No article filename set")

			relFilename = os.path.join(os.path.dirname(articleFilename), filename)

			page = cms.getPageBySource( relFilename )
			if page is None:
				cms.error("File or image %s not found", filename)
				raise KeyError("File or image not found")

			# TODO: Append correct arguments for preview pages
			url = page.getURL(kwds=kwds, websiteURL=websiteURL)
			if url is None:
				cms.error("Unable to generate URL for file/image %s", filename)
				raise KeyError("Unable to generate URL")

			return urlparse.urljoin(config.RSS_URL_PREFIX, url) # Absolute URLs!

		def __lookupTagURL__(tag):

			page = cms.getPageByTag(tag)
			if page is None:
				cms.error("Tag %s not found", tag)
				raise KeyError("Tag not found")

			url = page.getURL()
			if url is None:
				cms.error("Unable to generate URL for tag page")
				raise KeyError("Unable to generate URL")

			return url

		def __formatBBcode__(content): # Absolute URLs!
			return CDATAEscape( pureBBcode.htmlBBcode(	content, \
											previewMode 		= False, \
											showTableOfContents	= False, \
											staticURL 			= urlparse.urljoin(config.RSS_URL_PREFIX, config.CMSGEN_STATIC_URL), \
											websiteURL 			= urlparse.urljoin(config.RSS_URL_PREFIX, config.CMSGEN_WEBSITE_URL if websiteURL is None else websiteURL), \
											lookupInternalURL 	= __lookupInternalURL__, \
											lookupTagURL 		= __lookupTagURL__, \
											internalHostnames 	= config.BBCODE_INTERNAL_HOSTNAMES, \
											standaloneCSS 		= True
										) )

		if not isinstance(article, cmsArticle):
			raise TypeError("The page you specified is not derived from cmsArticle")

		template = buildTemplate(buildFile("templates/template-rss-article.xml"))
		template.assignElement("title",			buildRawData(article.getTitle(), formatFn=htmlEscape))
		template.assignElement("url",			buildRawData(urlparse.urljoin(config.RSS_URL_PREFIX, article.getURL(websiteURL=websiteURL)), formatFn=htmlEscape))		
		template.assignElement("author",		buildRawData(article.getAuthor(), formatFn=htmlEscape))
		template.assignElement("uuid",			buildRawData(article.getURL(websiteURL=websiteURL), formatFn=htmlEscape))
		template.assignElement("datetime",		buildRawData(article.getDateTime().strftime("%a, %d %b %Y %H:%M:%S %Z"), formatFn=htmlEscape))
		template.assignElement("description",	buildFile(article.getFilename(), skipHeader=True, formatFn=__formatBBcode__))

		# Generate list of tags
		tagList = buildList()
		for tag in article.getTags():
			tagList.appendElement(buildRawData(htmlFormat("<category>%s</category>", tag)))
		template.assignElement("categories",	tagList)

		return template

class cms(object):
	pageByURL       		= {} 	# url 		=> page
	pageBySource			= {} 	# filename 	=> page
	pageByTag				= {}	# tag 		=> first list page
	pages 					= set()

	invalidateSitemapURLs	= set() # All sitemaps which have been updated

	lock_uniqueURL     		= threading.RLock()
	lock_openUniqueFile 	= threading.RLock()

	def __init__(self):
		raise NotImplementedError("You cannot create an instance of cms")

	@staticmethod
	def debug(message, *args):
		sys.stderr.write(("\033[94m[DBG]\033[0m " + message + "\n") % args)

	@staticmethod
	def warning(message, *args):
		sys.stderr.write(("\033[93m[WRN]\033[0m " + message + "\n") % args)

	@staticmethod
	def error(message, *args):
		sys.stderr.write(("\033[91m[ERR]\033[0m " + message + "\n") % args)

	# Adds the given page to the list of pages - works for any object of cmsObject type
	@classmethod
	def __addPage__(cls, page):

		if not isinstance(page, cmsObject):
			raise TypeError("__addPage__ expects an object of type cmsObject")

		# We use a set here to prevent duplicate entries!
		cls.pages.add(page)

		# Register source file
		filename = page.getFilename()
		if filename is not None:
			filename = os.path.abspath(filename)
			cls.pageBySource[filename] = page

	# Generates a new article and adds it in one step
	@classmethod
	def addFilePage(cls, pageType, filename, *args, **kwds):
		filename = os.path.abspath(filename)

		if cls.pageBySource.has_key(filename):
			article = cls.pageBySource[filename]

			if not isinstance(article, pageType):
				return None

		else:
			article = pageType(filename, *args, **kwds) #readonly=readonly)
			cls.__addPage__( article )

		return article

	# Performs a lookup by URL
	@classmethod
	def getPageByURL(cls, url):
		try:
			return cls.pageByURL[url]
		except KeyError:
			return None

	# Lookup a page by the source path (absolute path)
	@classmethod
	def getPageBySource(cls, source):

		# Convert the path to an absolute path before doing the comparison
		source = os.path.abspath(source)

		try:
			return cls.pageBySource[source]
		except KeyError:
			return None

	# Returns the first tag page by a given tag string
	@classmethod
	def getPageByTag(cls, tag):
		try:
			return cls.pageByTag[tag]
		except KeyError:
			return None

	# Returns a list containing all pages of the website - please note that this also includes images!
	@classmethod
	def getPages(cls):
		return cls.pages

	# Ensures that all generated URLs are unique!
	@classmethod
	def uniqueURL(cls, url, page = None):

		def __iterURL__():
			yield url
			root, ext = posixpath.splitext(url)
			for index in itertools.count(2):
				yield "%s-%d%s" % (root, index, ext)

		with cls.lock_uniqueURL:
			for currentURL in __iterURL__():
				if not cls.pageByURL.has_key(currentURL):

					cls.pageByURL[currentURL] = page
					return currentURL

		# Will never occur!
		return None

	# Opens a unique file as a context manager and returns a handle (write mode)
	@classmethod
	@contextmanager
	def openUniqueFile(cls, filename):

		# Generate filenames by appending an index if the filename itself doesnt work
		def __iterFilename__():
			yield filename
			root, ext = os.path.splitext(filename)
			for index in itertools.count(2):
				yield "%s-%d%s" % (root, index, ext)

		# Main function ... using an idea from http://stackoverflow.com/questions/2961509/python-how-to-create-a-unique-file-name
		# In order to get this working on NFS I decided to implement a locking mechanism
		# See: http://unixhelp.ed.ac.uk/CGI/man-cgi?open+2	
		with cls.lock_openUniqueFile:
			for currentFilename in __iterFilename__():
				try:
					f = os.open(currentFilename, os.O_WRONLY | os.O_CREAT | os.O_EXCL, 0666)
				except OSError, exc:
					if exc.errno == errno.EEXIST:
						continue
					else:
						raise

				# Otherwise we return that one
				break

		# Generate a handle compatible with open(...)
		fp = os.fdopen(f, 'w')
		yield (filename, fp)
		fp.close()

	@classmethod
	def addInvalidateSitemapURL(cls, url):
		cls.invalidateSitemapURLs.add(url)

	@classmethod
	def invalidateSitemap(cls):
		global config

		if config.SITEMAP_INVALIDATE_URLS is None:
			return

		cls.debug("Invalidating sitemaps...")

		for host, queryURL in config.SITEMAP_INVALIDATE_URLS:

			# No connection established yet
			conn = None

			for url in cls.invalidateSitemapURLs:
				url = urlparse.urljoin(config.SITEMAP_URL_PREFIX, urlparse.urljoin(config.CMSGEN_WEBSITE_URL, url))

				# The following block might be repeated several times as the connection might be dropped in the meantime
				for i in xrange(2):

					if conn is None:
						conn = httplib.HTTPConnection(host)

					# Send new request
					try:
						conn.request('GET', htmlFormat(queryURL, url))
						break # Successful

					except (httplib.NotConnected, httplib.ImproperConnectionState, socket.gaierror):
						cls.warning("Connection dropped, trying to reconnect")

						conn.close()
						conn = None

				# Sending request only makes sense if we got a connection
				if conn is None:
					cls.error("Failed to invalidate sitemap of URL '%s'", url)
					continue

				# The response is a separate object
				response 	= conn.getresponse()

				cls.debug("Query with sitemap '%s' returned status: %d", url, response.status)

				# Read the remaining content
				while True:
					block = response.read(4096)
					if block == "": break

				# Delete the response object
				del response

				# Immediately continue with next URL, if we have luck the connection is still opened

			# Close connection if still opened
			if conn is not None:
				conn.close()

	# For each tag create a tag overview page
	@classmethod
	def generateTagPages(cls, elementsPerPage = 5):
		all_tags = {} # tag => [list_of_pages]

		# Find all used tags on all pages
		for page in cls.pages:
			if not isinstance(page, cmsHTMLPage):
				continue

			tags = page.getTags()
			for tag in tags:

				# Append the pages to the corresponding lists
				if not all_tags.has_key(tag):
					all_tags[tag] = []
				all_tags[tag].append(page)

		# For each tag
		for tag, pages in all_tags.iteritems():
			pages.sort(key=lambda x: (x.getDateTime(), x.getTitle()), reverse=True)

			# Calculate number of pages
			numberOfPages = (len(pages) + elementsPerPage - 1) / elementsPerPage

			# For each chunk of entries create a new page
			prevListOfPages = None
			for pageIndex, chunk in enumerate(chunkIter(pages, elementsPerPage)):
				listOfPages = cmsListOfPages(chunk, tag, pageIndex, numberOfPages)
				cls.__addPage__( listOfPages )

				# Create link between them
				if prevListOfPages is not None:
					prevListOfPages.setRefNextPage( listOfPages )
					listOfPages.setRefPrevPage( prevListOfPages )
				prevListOfPages = listOfPages

				# Add first page to list
				if pageIndex == 0:
					cls.pageByTag[tag] = listOfPages

	# Generate tag cloud page
	@classmethod
	def generateTagCloud(cls):

		# Generate the tag cloud
		tagCloud = cmsTagCloud()
		cls.__addPage__( tagCloud )

		# tagCloud.setMenubarTitle("Tag cloud")
		# tagCloud.setMenubarIndex(-2)

	# Generates the index page
	@classmethod
	def generateIndexPage(cls, elementsPerPage = 5):
		pages = []

		for page in cls.pages:
			if not isinstance(page, cmsHTMLPage):
				continue

			elif page.getShowInIndex():
				pages.append(page)

		pages.sort(key=lambda x: x.getDateTime(), reverse=True)

		# Calculate number of pages
		numberOfPages = (len(pages) + elementsPerPage - 1) / elementsPerPage

		# For each chunk of entries create a new page
		prevListOfPages 	= None

		for pageIndex, chunk in enumerate(chunkIter(pages, elementsPerPage)):
			listOfPages = cmsListOfPages(chunk, None, pageIndex, numberOfPages)
			cls.__addPage__( listOfPages )

			# Create link between them
			if prevListOfPages is not None:
				prevListOfPages.setRefNextPage( listOfPages )
				listOfPages.setRefPrevPage( prevListOfPages )
			prevListOfPages = listOfPages
	
			# First page should have a menubar entry
			if pageIndex == 0:
				listOfPages.setMenubarTitle("News")
				listOfPages.setMenubarIndex(0)

	@classmethod
	def generateRSSIndexPage(cls, elementsPerPage = 5):
		pages = []

		for page in cls.pages:
			if not isinstance(page, cmsHTMLPage):
				continue

			elif page.getShowInIndex():
				pages.append(page)

		pages.sort(key=lambda x: x.getDateTime(), reverse=True)

		# Calculate number of pages
		numberOfPages = (len(pages) + elementsPerPage - 1) / elementsPerPage

		# For each chunk of entries create a new page
		prevRSSListOfPages 	= None

		for pageIndex, chunk in enumerate(chunkIter(pages, elementsPerPage)):
			RSSListOfPages = cmsRSSListOfPages(chunk, pageIndex, numberOfPages)
			cls.__addPage__( RSSListOfPages )

			# Create link between them
			if prevRSSListOfPages is not None:
				prevRSSListOfPages.setRefNextPage( RSSListOfPages )
				RSSListOfPages.setRefPrevPage( prevRSSListOfPages )
			prevRSSListOfPages = RSSListOfPages

	# Generates the sitemap page
	@classmethod
	def generateSitemap(cls):

		# Get some information about all existing pages which should be shown in the index
		pages = []

		for page in cms.getPages():
			if not isinstance(page, cmsHTMLPage):
				continue

			elif page.getShowInIndex():
				pages.append(page)

		# generate sitemap object
		sitemap = cmsSitemap(pages)
		cls.__addPage__( sitemap )

	# Estimates the datetime when this website was updated the last time!
	@classmethod
	def getLastModified(cls):
		lastModified = None

		for page in cms.getPages():
			if not isinstance(page, cmsHTMLPage):
				continue

			elif not page.getShowInIndex():
				continue

			modificationTime = datetimeFromTimestamp(os.path.getmtime( page.getFilename() )) if page.getFilename() is not None else page.getDateTime()

			if lastModified is None or modificationTime > lastModified:
				lastModified = modificationTime

		return lastModified

	# Crawl the website directory and search for files which are generated by this CMS
	@classmethod
	def crawlWebsiteDirectory(cls, directory = None, crawlImages = False, readonly = True):
		global config

		if directory is None:
			directory = config.CMSGEN_WEBSITE_ROOT

		# Crawl through the directory
		for filename in os.listdir(directory):
			absFilename = os.path.join(directory, filename)
			relFilename = os.path.relpath(absFilename, config.CMSGEN_WEBSITE_ROOT) #cls.websiteRoot)

			# If the directory has the name "user-images" then enable crawlImages
			if os.path.isdir(absFilename) and filename not in [".git"]:
				cls.crawlWebsiteDirectory(absFilename, crawlImages=(crawlImages or (directory == config.CMSGEN_WEBSITE_ROOT and filename == "user-images")), readonly=readonly )

			# Nof file -> ignore
			elif not os.path.isfile(absFilename):
				continue

			# Try to determine the file type
			ext = os.path.splitext(filename)[1]
			if ext in (".html", ".htm", ".xml"):
				
				try:
					with open(absFilename, 'r') as fp:
						fp.readline() # Ignore first line containing doctype header
						cmsAttributes = decodeCMSComment(fp.readline())
						fp.close()

						# If it didnt match at all
						if cmsAttributes is None:
							cls.warning("File '%s' in the output directory doesn't contain a CMS-header!", absFilename)

							if cls.pageByURL.has_key(relFilename):
								raise RuntimeError("When continuing your file %s would be overwritten!" % absFilename)

							# We can safely proceed, and just ignore this file
							cls.pageByURL[relFilename] = None
							continue

						if cmsAttributes.has_key('HASH'):
							# Decode the hash
							try:
								lastDigest = cmsAttributes['HASH'].decode('hex')
							except TypeError:
								continue # Invalid hash, we cannot do anything

							# Okay, this file still exists - remember the digest for later usage
							if cls.pageByURL.has_key(relFilename):

								page = cls.pageByURL[relFilename]
								if  isinstance(page, cmsObject):
									page.setLastDigest(lastDigest)

							# WTF? File doesn't exist (anymore) - requires some updates
							elif not readonly:
								cls.debug("Deleting html/xml file '%s'", relFilename)

								# Unlink the file
								os.unlink(absFilename)

				except (IOError, OSError):
					cms.error("Unable to access html/xml file '%s'", absFilename)

			# For all image files
			elif crawlImages and ext in config.IMAGE_TYPES:

				try:
					modificationTime = os.path.getmtime(absFilename)

					if cls.pageByURL.has_key(relFilename):

						image = cls.pageByURL[relFilename]
						if  isinstance(image, internalImage):
							image.setLastModified(modificationTime)

					# Image doesn't exist (anymore)
					elif not readonly:
						cls.debug("Deleting own image '%s'", relFilename)

						# Unlink the image
						os.unlink(absFilename)

				except OSError:
					cms.error("Unable to access image file '%s'", absFilename)

	# Crawls the article directory and searches for files containing valid article headers
	@classmethod
	def crawlArticleDirectory(cls, directory = None, readonly = True):
		global config

		if directory is None:
			directory = config.CMSGEN_ARTICLE_SOURCE

		# This commands have to be processed as a last step -
		# as we have no control about the order of enumeration, we do this manually
		copyImageFiles = []

		# Crawl through the directory
		for filename in os.listdir(directory):
			absFilename = os.path.join(directory, filename)

			if os.path.isdir(absFilename) and filename not in [".git"]:
				cls.crawlArticleDirectory(absFilename, readonly)

			# For all text files
			elif not os.path.isfile(absFilename):
				continue

			# Try to determine the file type
			ext = os.path.splitext(filename)[1]
			if ext == ".txt":
				
				# Add pages to CMS
				try:
					cls.addFilePage(cmsArticle, absFilename, readonly=readonly)
				except IOError:
					cls.error("Unable to read article file '%s', skipping", absFilename)

			# For all image files
			elif ext in config.IMAGE_TYPES:

				# Add image to CMS
				try:
					cls.addFilePage(cmsImageOriginal, absFilename, readonly=readonly)
				except IOError:
					cls.error("Unable to read image file '%s', skipping", absFilename)

			# Commands to copy an image
			elif ext == ".copy-image":
				copyImageFiles.append( absFilename )

			# Ignore other types
			else: pass

		# Commands to copy an image (see above)
		for absFilename in copyImageFiles:

			# Lookup the commands to copy the image
			try:
				internalImage( absFilename )
			except IOError:
				cls.error("Unable to read copy-image file '%s', skipping", absFilename)

	# Performs the checks if all necessary infos are given, throws exceptions otherwise
	@classmethod
	def validateConfig(cls):
		global config

		if not isinstance(config.IMAGE_TYPES, (list, tuple, set, frozenset)):
			raise RuntimeError("IMAGE_TYPES should be list/tuple/set/frozenset")

		if not isinstance(config.BBCODE_INTERNAL_HOSTNAMES, (list, tuple, set, frozenset)):
			raise RuntimeError("BBCODE_INTERNAL_HOSTNAMES should be list/tuple/set/frozenset")

		if not isinstance(config.CMSGEN_DEFAULT_AUTHOR, basestring):
			raise RuntimeError("CMSGEN_DEFAULT_AUTHOR should be basestring")

		if not isinstance(config.CMSGEN_TIME_FORMAT, basestring):
			raise RuntimeError("CMSGEN_TIME_FORMAT should be a strftime format string")

		if config.CMSGEN_WEBSITE_ROOT is None or not os.path.isdir(config.CMSGEN_WEBSITE_ROOT):
			raise RuntimeError("CMSGEN_WEBSITE_ROOT should be the root directory of the website")

		if config.CMSGEN_STATIC_URL is None:
			raise RuntimeError("CMSGEN_STATIC_URL should be the (relative) URL to your website")

		if config.CMSGEN_WEBSITE_URL is None:
			raise RuntimeError("CMSGEN_WEBSITE_URL should be the (relative) URL to your website")
		if config.SITEMAP_URL_PREFIX is None:
			raise RuntimeError("SITEMAP_URL_PREFIX should be an absolute URL to your website root")

		if not isinstance(config.RSS_FEED_NAME, basestring):
			raise RuntimeError("RSS_FEED_NAME should be basestring")

		if config.RSS_URL_PREFIX is None:
			raise RuntimeError("RSS_URL_PREFIX should be an absolute URL to your website root")

		return True # Ready

	@classmethod
	def main(cls):
		""" Main function which generates and updates all pages contained in the CMS.
		"""
		global config

		# Validate config first!
		cls.validateConfig()

		# Crawl for more articles
		if config.CMSGEN_ARTICLE_SOURCE is not None:
			cls.crawlArticleDirectory(readonly=False)

		cls.debug("Generating tag and index pages...")
		cls.generateTagPages()
		cls.generateTagCloud()
		cls.generateIndexPage()
		cls.generateRSSIndexPage()
		cls.generateSitemap()

		cls.debug("Your website consists of %d pages", len(cls.pages))
		cls.debug("")

		cls.debug("Generting URLs for pages...")
		for page in cls.pages:
			page.generateURL()

		cls.debug("Identifying already existing pages ...")
		cls.crawlWebsiteDirectory(readonly=False)

		cls.debug("Updating html/xml pages...")
		for page in cls.pages:
			if isinstance(page, cmsTemplatePage):
				page.updatePage()

		cls.debug("Updating images...")
		for page in cls.pages:
			if not isinstance(page, cmsTemplatePage):
				page.updatePage()

		cls.debug("Invalidate sitemap...")
		cls.invalidateSitemap()

if __name__ == '__main__':
	cms.main()
