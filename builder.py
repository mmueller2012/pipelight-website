
import  os, os.path, hashlib, operator, re
from cStringIO import StringIO
import cPickle


# TODO: Drop buildTemplate formatFn functionality - not needed anymore

# global variables:
fileHashCache 			= {} # filename, lastDigest


class buildObject(object):
	def build(self, output = None):
		raise NotImplementedError("build method not implemented yet")

# List of buildObjects
class buildList(buildObject):
	def __init__(self):
		self.content = []

	def build(self, output = None):
		m = hashlib.md5()

		for element in self.content:
			m.update( element.build(output) )

		return m.digest()

	def appendElement(self, element):
		self.content.append(element)

# One external file
# formatFn(content)
class buildFile(buildObject):
	def __init__(self, filename = None, skipHeader = False, formatFn = None):
		self.filename 		= os.path.abspath(filename) if filename is not None else None
		self.skipHeader 	= skipHeader
		self.formatFn   	= formatFn

	def build(self, output = None):
		if self.filename == None:
			raise RuntimeError("No filename given for builded file")

		global fileHashCache

		try:
			modificationTime 	= os.path.getmtime(self.filename)
		except OSError:
			raise

		# No output requested, just return a cached hash if possible
		if output is None and fileHashCache.has_key(self.filename):
			lastModificationTime, lastDigest = fileHashCache[self.filename]
			if lastModificationTime == modificationTime:
				return lastDigest

		# build and/or calculate new hash
		try:
			m = hashlib.md5()

			with open(self.filename, "r") as fp:
				if self.skipHeader:
					while True:
						line = fp.readline().strip()
						if line == "":
							break
				content = fp.read()
				m.update(content)

				if output is not None:
					if self.formatFn:
						content = self.formatFn(content)
					output.write(content)

			digest = m.digest()

			# TODO: If adding the same file several times, don't update the hash?
			fileHashCache[self.filename] = ( modificationTime, digest )
			
			# Return the same digest
			return digest

		except IOError:
			raise

	def setFilename(self, filename):
		self.filename = os.path.abspath(filename)

	def setSkipHeader(self, skipHeader):
		self.skipHeader = skipHeader

	def setFormatFn(self, formatFn):
		self.formatFn = formatFn

# Some bytes of raw data or other objects!
# formatFn(content)
class buildRawData(buildObject):
	def __init__(self, rawData = None, formatFn = None):
		self.rawData 	= rawData
		self.formatFn 	= formatFn

	def build(self, output = None):
		content = self.rawData

		m = hashlib.md5()

		# Can we immediately create the md5sum of this? If not, use pickle to do it! :)
		if isinstance(content, basestring):
			m.update(content)
		else:
			m.update( cPickle.dumps(content, cPickle.HIGHEST_PROTOCOL) )

		if output is not None:
			if self.formatFn:
				content = self.formatFn(content)
				
			output.write(content)

		return m.digest()

	def setRawData(self, rawData):
		self.rawData = rawData

	def setFormatFn(self, formatFn):
		self.formatFn = formatFn

# Template object with placeholders
class buildTemplate(buildObject):
	def __init__(self, template = None):
		self.template 		= template
		self.keywords 		= {}
		self.resolveKeyword	= None
		self.re_template    = re.compile("\\$\\{([^ \t\n\r\f\v}]+)\\}")

	def build(self, output = None):
		if self.template is None:
			raise RuntimeError("No template source specified")

		elementBuildCache = {} # keyword => (hash, output)

		def __lookupKeyword__(r):
			keyword = r.group(1)

			# Try to return a cached value if available!
			try:
				cachedDigest, cachedString = elementBuildCache[keyword]
				return cachedString
			except KeyError:
				pass

			# Lookup the corresponding key if it exists
			if self.keywords.has_key(keyword):
				element, formatFn = self.keywords[keyword]

			elif self.resolveKeyword is not None:
				element, formatFn = self.resolveKeyword(keyword)

			else:
				return "<!-- MISSING KEY %s //-->" % keyword

			cachedIO 		= None
			cachedString 	= ""

			# Call the build method
			if output is not None:
				cachedIO = StringIO()

			cachedDigest = element.build(cachedIO)

			if cachedIO is not None:
				cachedString = cachedIO.getvalue()
				if formatFn:
					cachedString = formatFn(cachedString)

			elementBuildCache[keyword] = (cachedDigest, cachedString)

			if output is not None:
				return cachedString

			return ""

		m = hashlib.md5()

		templateIO = StringIO()
		m.update( self.template.build(templateIO) )

		# Perform the replacements! (also necessary if output = None)
		templateString = self.re_template.sub(__lookupKeyword__, templateIO.getvalue())

		if output is not None:
			output.write(templateString)

		for keyword, (cachedDigest, cachedOutput) in sorted(elementBuildCache.iteritems(), key=operator.itemgetter(0)):
			m.update(cachedDigest)

		return m.digest()

	def setTemplate(self, template):
		self.template = template

	def assignElement(self, keyword, element, formatFn = None):
		self.keywords[keyword] = (element, formatFn)

	def assignKwds(self, **kwds):
		for keyword, element in kwds.iteritems():
			self.keywords[keyword] = (element, None)

	def setResolveKeyword(self, resolveKeyword):
		self.resolveKeyword = resolveKeyword
