pipelight-website
=================

This repository hosts the content of pipelight.net, a website describing how to install and use Pipelight. To find out more about the pipelight project please visit: http://pipelight.net/cms/about.html

License
-------

As this repository contains several components, like the content management system itself and additionally also the source of articles and images, its not possible to release everything under the same license. Please take a look at the LICENSE file to find out more details about the license of specific files.

Using the CMS
-------------

At first you should install the following required dependencies (for example with 'sudo apt-get install' on Debian/Ubuntu):

* python2.7
* python-dateutil
* imagemagick (command line program 'convert')
* optipng
* gzip

After you have checked out the pipelight repository just run 'make' to build the website.

    :::bash
        git clone https://bitbucket.org/mmueller2012/pipelight-website.git
        cd pipelight-website
        make

When this is finished you can take a look at it - for example by starting a minimal http-server and opening it in your browser:

    :::bash
        ./http-server.sh
        # Now open http://localhost:8000/cms in your browser

When everything works you should now see the website, and can start to do your modifications and improvements. All source files are located in source/* - after changing them just run 'make' one more time, and everything should get updated. Please note that on the website pipelight.net a different HTTP server is used, so you should not rely on any features specific to this minimal HTTP server.

Contributing
------------
In most of the cases you want to change the content of the website itself. This can be done by adding or altering a **.txt** file inside of the **source** directory. All text files inside this directory will be converted to HTML and placed into the **cms** directory when running make. Please refer to one of the existing files for information about the structure of such a file. These files contain a header which describes general information about the content like the title or the author and the content of the site. You can find a description for our bbcode-like markup at http://fds-team.de/cms/bbcode-overview.html though it is a bit oudated and you may need to take a look at the source code of **pureBBcode.py** for the most up-to-date description.

Whenever you change something in an article make sure that you add yourself to the Author field since all the article content is licensed under the CC BY 3.0 license and this makes it easier for other people to name the authors. In case you are just fixing a small spelling issue or do not want to be named as author you can skip this step, though your attribution will be lost when someone else uses the content since there is most probably no git log any more. If you are working on an existing file and the Author field does not exist yet, please add one in the following way:

    Author: Pipelight Team, YOUR NAME

If you are changing something inside the code of the CMS (which is licensed under the BSD 2-Clause License) you should add yourself to the AUTHORS file.

Submitting changes
------------------

Please make sure that you entered the correct author information in git before committing your changes. When you are finished with all your changes either submit them as a patch-file to webmaster@fds-team.de or alternatively send a pull-request. In case you want to send us a patch file make sure that you export your patch in the following way:

    :::bash
        git format-patch --keep-subject HEAD^
        
This ensures that the subject and the author information is saved inside the patch so that we can directly apply it.