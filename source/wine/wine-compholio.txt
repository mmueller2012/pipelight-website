Title: 			Wine-Compholio
Menubar-Index:	4
Tags:			Silverlight, Pipelight, NPAPI, Linux, Plugin, Wine, Flash
Show-in-Index:	no
Menubar-Title:	Wine
Image:			../pipelight-logo.svg

[warning]
This site is heavily out of date. Please take a look at [url=http://wine-staging.com]wine-staging.com[/url] for more recent information.
[/warning]

[preview]
Pipelight unfortunately still requires a custom Wine version (called Wine-Compholio) to run properly, as our patches are not all upstream yet (and it is unlikely that all of them will get upstream soon). This page should give a short overview about how to compile Wine and which patches are absolutely necessary. 

Please note that the instructions provided here are not complete, they should only be a hint for developers and users who are experienced in building software on their own and know how to do compile 32 bit programs under 64 bit. The most problematic part of building Wine is to get the necessary 32 bit dependencies installed, these dependencies are very distribution-specific and finding and installing them is not explained here. You should take a look at the Wine source packages which are already available for your distribution and check which build dependencies are listed there and install them. Alternatively you can also take a look at the corresponding Wine [url=http://wiki.winehq.org/WineOn64bit]wiki entry[/url] which explains the necessary steps and dependencies, but has the disadvantage that it is not always up up to date. You can try to ask us when you have trouble to compile Wine, but as many of these problems are distribution-specific we may not always be able to help you.
[/preview]

[section]Wine patches[/section]

As we have switched from Bzr to Git to maintain our patches there is no need to use the automatically generated tarballs anymore - github automatically creates tarballs for every tag we create in our repository. We recommend to refer to the following locations for the latest release and upstream version of the patches.

[table nobox center]

[row]
[* colspan=2] [b]Latest release (tag: v1.7.23)[/b]
[/row]

[row]
[*] Tarball
[*] [code]wget https://github.com/compholio/wine-compholio-daily/archive/v1.7.23.tar.gz[/code]
[/row]

[/table]

[table nobox center]

[row]
[* colspan=2] [b]Current upstream source[/b]
[/row]

[row]
[*] Git via HTTP
[*] [code]git clone https://github.com/compholio/wine-compholio-daily.git[/code]
[/row]

[/table]

For those that still use the old method and manually track each patch: We recommend to switch over to the new method, since this makes a lot of things easier. The old method has especially the following problems:

[list]
[*] The [url=https://github.com/compholio/wine-compholio-daily/tree/master/patches/10-Missing_Fonts]binary font patches[/url] will be [b]silently ignored[/b] when you apply them with [code]patch -p1[/code].
[*] The patch-list (which can be shown later at runtime with [code]wine --patches[/code]) will probably not be up-to-date, when you manually select which patches you want. This might lead the users to a wrong impression, that their version is fully compatible, even if it isn't.
[*] The number of patches and their numbering may change at any time if a patch is accepted or needs to be updated. Manually updating a huge list of patches will easily get complicated...
[/list]

Installation with the Makefile included in our tarball automatically takes care of this, and also makes updating a lot easier. If you nevertheless want to use the old method, here are the links. Be warned that we will probably discontinue updating them in the future.

[download]
[* url="http://fds-team.de/mirror/wine-patches.tar.gz"] The same as above, but without definition files and without subfolders (auto-generated)
[* url="http://fds-team.de/mirror/wine-patches-minimal.tar.gz"] Tarball containing the minimum required patches (auto-generated)
[/download]

For an older wine version (not recommended, as they might contain additional bugs or the patches probably don't apply) you additionally have to grab the patches that are already upstream. The easiest way to grab them is by using the download links below which point directly to the corresponding git commits. Feel free to ask us (for example via IRC, our channel is #pipelight on IRC freenode) when you're unsure which patches are absolutely necessary, the same applies also to the case when you want to build a newer version.

[section]Building 32 bit Wine[/section]

The following chapter gives a short summary on how to build the required 32 bit version of Wine. Make sure that you have installed the necessary 32 bit development files as described in your distribution specific Wine package or at the [url=http://wiki.winehq.org/WineOn64bit]Wine wiki entry[/url].

[b]NOTE[/b]: [i]Besides the normal dependencies, you will additionally have to install the development files of libattr (i386) to get DRM working with Silverlight. Please double check that Wine was able to detect the libattr header files![/i]

Example instructions for [code]wine-1.7.23[/code]:

[code=bash]
# Download the wine source and the patches
wget http://prdownloads.sourceforge.net/wine/wine-1.7.23.tar.bz2
wget https://github.com/compholio/wine-compholio-daily/archive/v1.7.23.tar.gz

# Extract the archives
tar xvjf wine-1*.tar.bz2 
cd wine-1*
tar xvzf ../v1.7.23.tar.gz --strip-components 1

# Apply the patches
make -C ./patches DESTDIR=$(pwd) install

# Run configure
./configure --with-xattr
# or: ./configure --with-xattr --prefix=$HOME/mywine

# Build
make

# Install
sudo make install
# or, when your prefix is not owned by root: make install
[/code]

Depending on where you install your custom wine version, you will have to specify the same path in the pipelight configure step (or modify the configuration files accordingly), such that Pipelight is able to find it.

[section]Patch status[/section]

This table should provide you with the current status of all patches used by Pipelight. It will be updated when our patches are upstream or have been updated.

Please note that we will only list all directly Pipelight related patches here - other features and improvements of Wine which we do in our spare time will not be listed here.

[table center nobox]

	[row]
		[* center] [b]Maintainer[/b]
		[* center] [b]Patch[/b]
		[* center] [b]Status[/b]
		[* center] [b]Download[/b]
	[/row]

	[row]
		[* colspan=4] [center][b]Already upstream[/b][/center]
	[/row]

	[row]
		[*] Erich E. Hoover
		[*] setupapi: Close the target file before issuing SPFILENOTIFY_FILEEXTRACTED.
		[*] [color=green]upstream in 1.5.15[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/f036f462174d047e960bded01bef48dd7e0c19c2]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] setupapi: Report the correct target file with SPFILENOTIFY_FILEEXTRACTED.
		[*] [color=green]upstream in 1.5.15[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/2ee8cd5a6c15af5890145d9c05c31cbeca592eec]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] ntdll: Implement nanosecond precision file time storage.
		[*] [color=green]upstream in 1.5.17[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/5c95bec8478fe64611d86ed8f376e906329588bb]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] wininet: Fix InternetCrackUrl parsing URLs containing a semicolon.
		[*] [color=green]upstream in 1.5.18[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/499c53175bd9e886dcbf02d0a70bad0d1ac0b900]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] advapi32: Implement SetSecurityInfo on top of NtSetSecurityObject.
		[*] [color=green]upstream in 1.5.18[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/d5e40b0eff65aa2d309b9e4be7d9867fd1cf7eb8]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] advapi: Implement GetNamedSecurityInfoW on top of GetSecurityInfo.
		[*] [color=green]upstream in 1.5.19[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/722c2b1008416ff72be4e1d60060e79957061584]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] advapi: Implement SetNamedSecurityInfoW on top of SetSecurityInfo.
		[*] [color=green]upstream in 1.5.19[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/5851032d07c321f2c050187115cfd8e21b5f3f49]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] user32: SetTimer and SetSystemTimer should respect the timeout limits.
		[*] [color=green]upstream in 1.5.30[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/057b0d8bcac633ff80238c6de3ece04655eccf2c]patch[/url]	
	[/row]
	[row]
		[*] Michael Müller
		[*] d3d9: Add headers for IDirect3DSwapChain9Ex interface.
		[*] [color=green]upstream in 1.7.2[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/3c759894970f6692605108d5e0df1eeed2f0e5bb]patch[/url]
	[/row]
	[row]
		[*] Michael Müller
		[*] d3d9: Partial implementation of IDirect3DSwapChain9Ex.
		[*] [color=green]upstream in 1.7.3[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/edad7dea7e09cb48daf0e36044ffb99a0588f4fa]patch[/url]
	[/row]
	[row]
		[*] Michael Müller
		[*] d3d9/tests: Test if IDirect3DSwapChain9Ex is available with IDirect3D9(Ex).
		[*] [color=green]upstream in 1.7.3[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/cf01bb3d2ab711c9a0504350097383372b8f6818]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] d3d9/tests: Implemented tests for IDirect3DSwapChain9Ex_GetDisplayModeEx.
		[*] [color=green]upstream in 1.7.3[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/0fb3aaac1044ee0020ddf7654f838ed008dd0bff]patch[/url]
	[/row]
	[row]
		[*] Michael Müller
		[*] quartz: Pass correct interface to IBaseFilter::JoinFilterGraph in function FilterGraph2_AddFilter.
		[*] [color=green]upstream in 1.7.3[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/2fcfde0f627e0e466c897d47b4793e851faf2d44]patch[/url]
	[/row]
	[row]
		[*] Andrew Eikum
		[*] oleaut32: Implement VarDecRound. [i](by codeweavers)[/i] 
		[*] [color=green]upstream in 1.7.3[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/87c459ab2359784f238c30073b1adc0111876987]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] winex11: Added missing release_win_data() to create_foreign_window().
		[*] [color=green]upstream in 1.7.3[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/5fb74c48d10e1f511adc43caf44d09e489c5c29e]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] winex11: Call destroy_gl_drawable before destroying the window.
		[*] [color=green]upstream in 1.7.4[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/e915cfd4e7ca78369a17172891b5a5f8e442d880]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] winex11: Call sync_context for DC_GL_CHILD_WIN drawables before swapping buffers.
		[*] [color=green]upstream in 1.7.4[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/4e9646a7f74da5f91b164b0b0a411b4b1f3c731d]patch[/url]
	[/row]
	[row]
		[*] André Hentschel
		[*] include: Add some definitions to axextend.idl.
		[*] [color=green]upstream in 1.7.6[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/b7672fac6dba4ccb2aaa1ddad678e8946a5653aa]patch[/url]
	[/row]
	[row]
		[*] André Hentschel
		[*] quartz: Get rid of the VMR9Impl typedef.
		[*] [color=green]upstream in 1.7.6[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/bc59206e0e3c70c25c6230f9a22c61cb79b532e0]patch[/url]
	[/row]
	[row]
		[*] André Hentschel
		[*] quartz: Add Video Mixing Renderer 7.
		[*] [color=green]upstream in 1.7.6[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/f9cccaa3def66a288117a7204ba2ab1b1383c579]patch[/url]
	[/row]
	[row]
		[*] André Hentschel
		[*] quartz: Add stubbed IVMRFilterConfig interface.
		[*] [color=green]upstream in 1.7.6[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/d029181d1a049b8e46054948ce0e02b947ded25d]patch[/url]
	[/row]
	[row]
		[*] André Hentschel
		[*] quartz: Add stubbed IVMRWindowlessControl interface.
		[*] [color=green]upstream in 1.7.6[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/e26d4a6f43dd508e81f7bfd15418cdcf087c98d2]patch[/url]
	[/row]
	[row]
		[*] André Hentschel
		[*] quartz: Add stubbed IVMRSurfaceAllocatorNotify interface.
		[*] [color=green]upstream in 1.7.6[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/abbb40fa032535d317650576ea88d28811e7ae4e]patch[/url]
	[/row]
	[row]
		[*] André Hentschel
		[*] quartz: Add stubbed IVMRMonitorConfig interfaces.
		[*] [color=green]upstream in 1.7.6[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/ab925b39b4eb2a9decca14ea0938e7af087949c4]patch[/url]
	[/row]
	[row]
		[*] André Hentschel
		[*] quartz: Add stubbed IAMCertifiedOutputProtection interface.
		[*] [color=green]upstream in 1.7.6[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/217faec5f59c5a83981c39698e94391a7bd3d766]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] shlwapi: Fix UrlCombineW for URLs containing a quotation mark.
		[*] [color=green]upstream in 1.7.6[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/7ee2c24a8cb0ea3ca50d62634fc5772438c2ae39]patch[/url]
	[/row]
	[row]
		[*] Dmitry Timoshkov
		[*] kernel32: Add special case for "." and ".." to GetLongPathName.
		[*] [color=green]upstream in 1.7.6[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/eba2f4322169715b3672651a5427b718b6eafa8f]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] winex11: Implement additional XEMBED events.
		[*] [color=green]upstream in 1.7.7[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/df6dc091e6524581549d65c04c61f5ccd22dfe48]patch[/url]
	[/row]
	[row]
		[*] Michael Müller
		[*] quartz: Improve stubs for AMCertifiedOutputProtection.
		[*] [color=green]upstream in 1.7.7[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/1b5026424b767d699c8c9f1ac743ddb357d16ae3]patch[/url]
	[/row]
	[row]
		[*] Michael Müller
		[*] quartz: Partial implementation of VMR7MonitorConfig and VMR9MonitorConfig.
		[*] [color=green]upstream in 1.7.7[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/815f252b4a91bf83e69ae8b69defaafe6f7c68a1]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] winex11: Send XEMBED_REQUEST_FOCUS request for embedded windows.
		[*] [color=green]upstream in 1.7.7[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/2e0ca3e74669b55f5d64b39e8fd32dc052652d39]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] advapi32/tests: Add tests for creating directories with specific security attributes.
		[*] [color=green]upstream in 1.7.7[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/95c414fea7dd17afe81de406f0a26a29e74cece6]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Create directories with the specified security attributes.
		[*] [color=green]upstream in 1.7.7[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/d2e216f462c2ccc57cac6c8e683aae2abd719916]patch[/url]
	[/row]
	[row]
		[*] Michael Müller
		[*] quartz: Return increasing monitor GUID on VMR7 monitor enumeration.
		[*] [color=green]upstream in 1.7.10[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/6e373aa6956c8bc267c0051cb04f6a34574cca5a]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] winex11: Catch invalid values from broken QX11EmbedContainer implementations.
		[*] [color=green]upstream in 1.7.12[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/f3568a5e1222ce2a85a05efeb417c141fceb0d78]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] kernel32: Add support for security access parameters for named pipes.
		[*] [color=green]upstream in 1.7.13[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/ee49a5a273e993684ae49c4664c1620d06da3ec2]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] shell32: Register user administrative tools shell folder.
		[*] [color=green]upstream in 1.7.15[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/e28fe966feb999ac1d39669c2e8b0941b86cecc5]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] ws2_32: Ask the server to process unsupported WSAIoctl operations.
		[*] [color=green]upstream in 1.7.16[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/5963d7f09c0b83e01dec6787dd0aceb6ee610d81]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] ntdll: Unify exception function lookup on x86_64.
		[*] [color=green]upstream in 1.7.17[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/a71d1e389f7fffb66b93de3a9a9bbb568337623a]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] ntdll: Implement RtlAddFunctionTable / RtlDeleteFunctionTable for x86_64.
		[*] [color=green]upstream in 1.7.17[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/98307b22fba5c0d757be15b9f355b29e97ab1d5c]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] ntdll: Implement RtlInstallFunctionTableCallback on x86_64.
		[*] [color=green]upstream in 1.7.17[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/4d8edf7612a62e1d2234f310b060e03e920f540c]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] ntdll/tests: Add tests for dynamic unwind table.
		[*] [color=green]upstream in 1.7.17[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/7821be356d4c3df1e532427d8241396725e974b1]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] kernel32: Forward RtlInstallFunctionTableCallback to ntdll.
		[*] [color=green]upstream in 1.7.17[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/7ba43b2fd1b7e62e12018f65056907e1303d18a3]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] kernel32/tests: Add a variety of tests for CompareStringEx.
		[*] [color=green]upstream in 1.7.17[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/4b949510fbed87b9af997a280010392b45bc9d81]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] kernel32: Allow CompareStringEx NORM_LINGUISTIC_CASING flag.
		[*] [color=green]upstream in 1.7.17[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/b0935ab8111323a01fdc837a274deeda93263f8e]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] kernel32: Allow CompareStringEx LINGUISTIC_IGNORECASE flag.
		[*] [color=green]upstream in 1.7.17[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/32da6626f562146c0fd1d8550c60995359b2a0da]patch[/url]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] user32: Reduce the minimum Set[System]Timer from 15 ms to 10 ms.
		[*] [color=green]upstream in 1.7.18[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/b5bd529769696f2b4e4f7d6389f2dc21da5acd4a]patch[/url]
	[/row]
	[row]
		[*] Michael Müller
		[*] server: Fix return value for FSCTL_PIPE_WAIT if pipe does not exist.
		[*] [color=green]upstream in 1.7.20)[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/9602fa7a9317f1bf856fe441dd0aeb7d0cbfa9fd]patch[/url]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] ntdll/tests: Fix exception test failures on x86_64.
		[*] [color=green]upstream in 1.7.20[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/dd0e9fe91e647ee5ce95709b7f47b2e1e782a3c9]patch[/url]
	[/row]
	[row]
		[*] Michael Müller
		[*] ntdll: Stub TokenAppContainerSid in NtQueryInformationToken.
		[*] [color=green]upstream in 1.7.20[/color]
		[*] [url=http://source.winehq.org/git/wine.git/patch/97c3bb040e7811fcd506fb5c29f5bdbec4ffead5]patch[/url]
	[/row]

	[row]
		[* colspan=4] [center][b]Additional patches (wine-1.7.23)[/b][br][i](upstream versions might differ when changes or additional tests were necessary!)[/i][/center]
	[/row]

	[row]
		[*] Erich E. Hoover
		[*] server: Implement socket-specific ioctl() routine.
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Add socket-side support for the interface change notification object.
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Add blocked support for SIO_ADDRESS_LIST_CHANGE ioctl().
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Implement the interface change notification object.
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] ws2_32: Add an interactive test for interface change notifications.
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Unify the storage of security attributes for files and directories.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Unify the retrieval of security attributes for files and directories.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Store file security attributes with extended file attributes.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Store user and group inside stored extended file attribute information.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Retrieve file security attributes with extended file attributes.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Convert return of file security masks with generic access mappings.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Inherit security attributes from parent directories on creation.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Inherit security attributes from parent directories on SetSecurityInfo.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] shell32: Set the default security attributes for user shell folders.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Add compatibility code for handling the old method of storing ACLs.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] server: Add additional checks in get_xattr_sd to prevent crashes caused by invalid attributes.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Michael Müller
		[*] server: Add support for ACLs/xattr on FreeBSD
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] winex11: Update gl_drawable for embedded windows
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] winex11: Enable/disable windows when they are (un)mapped by foreign applications
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] kernel32: Change return value of stub SetNamedPipeHandleState to TRUE
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] fonts: Add a subset of Liberation Sans as an Arial replacement.
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] fonts: Implement the rest of Liberation Sans (Arial replacement).
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] ntdll: Use lockfree implementation for get_cached_fd.
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Add default security descriptor ownership for processes.
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] server: Add default security descriptor DACL for processes.
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] winex11: Implement X11DRV_FLUSH_GDI_DISPLAY ExtEscape command
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Michael Müller
		[*] user32: Decrease minimum SetTimer interval to 5 ms
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] quartz/tests: Add tests for IVMRMonitorConfig and IVMRMonitorConfig9 interface
		[*] [color=orange]in progress[/color]
	[/row]
	[row]
		[*] Erich E. Hoover
		[*] wined3d: Silence repeated resource_check_usage FIXME.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] kernel32: Silence repeated CompareStringEx FIXME.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] wined3d: Silence repeated wined3d_swapchain_present FIXME.
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] shlwapi/tests: Add additional tests for UrlCombine and UrlCanonicalize
		[*] [color=gray]not sent upstream yet[/color]
	[/row]
	[row]
		[*] Sebastian Lackner
		[*] shlwapi: UrlCombineW workaround for relative paths
		[*] [color=gray]not sent upstream yet[/color]
	[/row]

[/table]
