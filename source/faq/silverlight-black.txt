Title: 			FAQ - Silverlight shows just a blank rectangle / Netflix error 1001
Image:			../help/help.svg
Show-in-Index:	no
Group:			faqs
Tags:			Blank rectangle, N1001, Core fonts
URL:			faqs/faq-silverlight-shows-just-a-blank-rectangle-netflix-error-1001.html

When you've installed Pipelight and notice that the Silverlight plugin shows just a blank rectangle (but the right click popup menu works as excepted and you see the correct Silverlight version there), one possible reason is that you are either missing libXComposite or the microsoft core fonts. You can check both problems by executing our system check utility:

[code=bash]pipelight-plugin --system-check[/code]

In case you are missing one of these libraries you should use your package manager and install them. If the fonts can not be located by our check program, you can still have them installed but didn't accept the license during the installation.

The following section explains on how to check this for Debian based systems. Please refer to the according documentation of the core fonts package for other your distributions.

To check if the ttf-mscorefonts-installer package is installed use:
[code=bash]dpkg --list | grep ttf-mscorefonts-installer[/code]

To verify if you accepted the EULA:
[code=bash]debconf-show ttf-mscorefonts-installer 2>/dev/null | grep "msttcorefonts/accepted-mscorefonts-eula:"[/code]

In case the package is not installed yet OR you haven't accepted the EULA (...: false) please use the following command to (re)install it:
[code=bash]sudo apt-get install --reinstall ttf-mscorefonts-installer[/code]

During the installation you will be asked whether you want to accept the EULA. Use TAB to select the correct button and accept the license by pressing ENTER.