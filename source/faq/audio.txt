Title: 			FAQ - Videos play very fast / lag or don't have sound
Image:			../help/help.svg
Show-in-Index:	no
Group:			faqs
Tags:			Audio, Sound, PulseAudio, ALSA, Fast-forward, Lag
URL:			faqs/faq-videos-play-very-fast-lag-or-dont-have-sound.html

Many of the performance problems, like huge framedrops, are often caused by the sound playback as plugins like Silverlight or Flash synchronize the displayed video frames to the audio output. This is generally not a bad idea to achieve a proper lip sync, but often causes problems when Pulseaudio is used as sound server. If you encounter such a problem and verified that you have installed your graphic drivers properly, as described in our [aurl="graphic.txt"]graphic FAQ entry[/aurl], you may want to take a look at the following tips.

[section]Restart Pulseaudio[/section]
In case the problem does not occur too often, you can probably fix it by simply restarting Pulseaudio:

[code=bash]pulseaudio -k[/code]

Unfortunately you'll have to run this again whenever you experience the problem and it may also be necessary to restart the plugin by reloading the page in your webbrowser.

[section]Reduce fragment size in Pulseaudio[/section]

In case the step above does not help you and you have extremely laggy playback, you may try reducing the fragment size in the Puselaudio configuration. This is known to completely fix the problem on some systems. Just open the Pulseaudio config file with a text editor and root rights, for example by using:

[code=bash]sudo gedit /etc/pulse/daemon.conf[/code]

Now change the fragment size value:
[code=text]default-fragment-size-msec = 5[/code]

You may need to uncomment the line, i.e. remove the ';' at the beginning. Now restart Pulesaudio with [code]pulseaudio -k[/code] and test again.

[section]Switch to ALSA emulation[/section]

Our patched Wine version directly supports Pulseaudio to increase performance, but some users had problems which could be solved by switching back to the old Pulseaudio ALSA emulation layer.

Before you finally change over to ALSA, you may want to checkout the Wine audio settings by executing:

[code=bash]WINEPREFIX=~/.wine-pipelight WINEARCH=win32 /usr/share/pipelight/wine winecfg[/code]

and changing to the audio tab. Try to switch the audio output device and use the test button to verify whether you can hear the played sound.

If this still doesn't help, you can set the audio output to alsa with:

[code=bash]
wget -O ~/.wine-pipelight/winetricks http://winetricks.org/winetricks
chmod +x ~/.wine-pipelight/winetricks
WINEPREFIX=~/.wine-pipelight WINE=/usr/share/pipelight/wine WINEARCH=win32 ~/.wine-pipelight/winetricks
[/code]

Choose: "Select the default wineprefix" -> "Change Wine settings" -> "sound=alsa"

Try changing the audio settings again if it still does not work.

[section]Reverting changes[/section]

If you want to revert all settings, you can simply delete the Wine prefix and everything will be reinstalled on the next browser start:

[code=bash]rm -rf ~/.wine-pipelight[/code]

[warning] Some VOD services limit the number of different devices you are allowed to use within a specified amount of time. Deleting the wine-prefix will cause your system to be identified as new device and therefore decrease the number of devices left. We recommend to create a backup of the prefix in this case, so that you can keep your device identification.[/warning]