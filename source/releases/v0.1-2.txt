Title:			Release v0.1-2
Show-in-Index:	yes
Image:			update.svg
URL:	articles/2014-05/release-v0-1-2.html
DateTime:	2013-08-28T14:20:34.243770+00:00

[preview]
We just released Pipelight v0.1-2 with a lot of new features and bug fixes.
The following text gives a short summary about the most important changes:
[/preview]

[faq]

[*="System wide installation"]
By default Pipelight is installed systemwide - if you don't like this you can now switch to a non-system-wide installation. How to do that is described in the following FAQ: https://answers.launchpad.net/pipelight/+faq/2362

[*="GPU acceleration support"]
As we've fixed the corresponding bug in wine (not upstream yet) its now possible to use hardware acceleration. In the default configuration GPU acceleration is disabled as it may cause problems with some drivers. We are currently working on a detection script which will become active on the next silverlight-installer update, that enables hardware acceleration per default on known-to-work setups, . Moreover we only activate hardware acceleration on sites that request it, but you can manually change these settings (force/disable GPU acceleration) as described here: https://answers.launchpad.net/pipelight/+faq/2364

Please note: When you are using some graphic card drivers it might be possible that GPU acceleration doesn't work correctly, but unfortunately there is nothing we can do about this, except hoping, that the graphic drivers and wined3d9 will improve even further.

[*="Fixed XEMBED bug / automatic windowlessmode detection"]
In previous versions it was still required to manually change values in the config (especially 'xembed' and 'windowlessmode') to get Pipelight working on some sites / desktop enviroments. These workarounds are no longer needed since the corresponding bugs are fixed now. If you do not want to force some specific behavior you should simply delete/rename your ~/.config/pipelight file and test if it works with the default config. Using the standard config is preferred as it allows us to change the default values / add additional features on an update.

[*="Implemented diagnostic methos"]
If you just see "Pipelight error" its often difficult to figure out where the problem is exactly - thats why we added some code to automatically detect common errors. Just go to http://fds-team.de/pipelight and it will run some diagnostic checks that might help you to find out where the problem is.

[*="Better browser support"]
It's now possible to use Pipelight in combination with Midori, Opera and even Uzbl!
In case you want to give Uzbl a try: Our FAQ page https://answers.launchpad.net/pipelight/+faq/2351 contains the required user agent settings.

[/faq]

If you want to see all the changes in detail please take a look at the the full changelog:
https://bitbucket.org/mmueller2012/pipelight/raw/master/debian/changelog