Title: 			Installation - CentOS
Image:			../download.svg
Show-in-Index:	no
Group:			install
URL:	install/installation-centos.html

We provide a repository with CentOS 6 packages, so that you do not need to care about Pipelight or Wine updates. Just add the repository with the following command:
[code=bash]
sudo wget http://download.opensuse.org/repositories/home:/DarkPlayer:/Pipelight/CentOS_CentOS-6/home:DarkPlayer:Pipelight.repo -O /etc/yum.repos.d/pipelight.repo
[/code]

Now you should be able to install Pipelight as any other package. We also recommend to update the plugin database of Pipelight after the installation.
[code=bash]
sudo yum install pipelight
sudo pipelight-plugin --update
[/code]

The next step is to [aurl=../installation.txt#section_2]enable the plugins[/aurl] you want to use.
