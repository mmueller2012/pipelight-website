Title: 			Installation
Menubar-Index:	2
Tags:			Silverlight, Pipelight, NPAPI, Linux, Plugin, Wine, Flash
Image:			download.svg
Show-in-Index:	no
Menubar-Title:	Installation

[preview]
This page contains information about how to install Pipelight either via a precompiled package on supported distributions or how to compile it yourself. To use Pipelight you first need to install the package itself and then enable the plugins you want to use. For some websites you will also need to install a user agent switcher in your browser. [b]Note:[/b] Pipelight does not work in Chrome / Chromium version 35 or higher, take a look at our [aurl=../chromium/chromium.txt]Chromium page[/aurl] for more information.
[/preview]

[section]Package Installation[/section]

The first step is to install the Pipelight package and dependencies on your system. Click on one of the supported systems to see the distribution specific steps. Please note that (unless explicitly stated otherwise) these packages are provided by the Pipelight team, and not the distributions themselves.

[table center nobox nominwidth]

[row]
	[* center] [aimg height=2em]distros/ubuntu.svg[/aimg]
	[*]	[aurl=distros/ubuntu.txt]Ubuntu[sup]®[/sup][/aurl]
[/row]

[row]
	[* center] [aimg height=2em]distros/arch.svg[/aimg]
	[*] [aurl=distros/arch.txt]Arch Linux[sup]®[/sup][/aurl]
[/row]

[row]
	[* center] [aimg height=2em]distros/debian.svg[/aimg]
	[*] [aurl=distros/debian.txt]Debian[sup]®[/sup] Wheezy / Jessie / Stretch / Sid[/aurl]
[/row]

[row]
	[* center] [aimg height=2em]distros/opensuse.svg[/aimg]
	[*] [aurl=distros/opensuse.txt]openSUSE[sup]®[/sup][/aurl]
[/row]

[row]
	[* center][aimg height=2em]distros/fedora.svg[/aimg]
	[*][aurl=distros/fedora.txt]Fedora[sup]®[/sup] 18 / 19 / 20 / 21[/aurl]
[/row]

[row height=2em]
	[* center][aimg height=2em]distros/avlinux.png[/aimg]
	[*][aurl=distros/avlinux.txt]AVLinux[/aurl]
[/row]

[row]
	[* center][aimg height=2em]distros/slackware.jpg[/aimg]
	[*][aurl=distros/slackware.txt]Slackware[sup]®[/sup][/aurl]
[/row]

[row height=2em]
	[* center][aimg height=2em]distros/steamos.png[/aimg]
	[*][aurl=distros/steamos.txt]SteamOS[sup]®[/sup][/aurl]
[/row]

[row]
	[* center][aimg height=2em]distros/mageia.svg[/aimg]
	[*][aurl=distros/mageia.txt]Mageia[sup]®[/sup] 4[/aurl]
[/row]

[row]
	[* center][aimg height=2em]distros/centos.svg[/aimg]
	[*][aurl=distros/centos.txt]CentOS[sup]®[/sup] 6[/aurl]
[/row]

[row]
	[* center][aimg height=2em]distros/freebsd.png[/aimg]
	[*][aurl=distros/freebsd.txt]FreeBSD[sup]®[/sup][/aurl]
[/row]

[row]
	[* center][aimg height=2em]distros/pcbsd.png[/aimg]
	[*][aurl=distros/pcbsd.txt]PC-BSD[sup]®[/sup][/aurl]
[/row]

[row]
	[* center][aimg height=2em]distros/sabayon.svg[/aimg]
	[*][aurl=distros/sabayon.txt]Sabayon[sup]®[/sup][/aurl]
[/row]


[/table]

If there are no ready-to-use packages for your distribution, you can still compile Pipelight [aurl=distros/compile.txt]on your own[/aurl].

[section]Using windows browser plugins[/section]

After installing Pipelight, you will need to enable the plugins you want to use. By default all plugins are disabled and you need to enable them through the [code]pipelight-plugin[/code] script. Enabling a plugin is as easy as executing the following command in your terminal:

[code=bash]
sudo pipelight-plugin --enable pluginname
[/code=bash]

To disable the plugin again use:

[code=bash]
sudo pipelight-plugin --disable pluginname
[/code=bash]

If you do not want to enable a plugin for all users on your system, but only your current user, simply leave out the [code]sudo[/code]. The [code]pipelight-plugin[/code] script supports a lot more commands, which you can check out in our [url="/cms/misc/pipelight-plugin.html"]man page[/url].

Not all plugins work completely out of the box and may require some additional configuration or steps. Therefore make sure to checkout the plugin related information:

[table center nobox nominwidth]
	[row] 
		[*][aimg height=2em]plugins/video.svg[/aimg]
		[*][aurl="plugins/silverlight.txt"]Silverlight[sup]®[/sup][/aurl]
	[/row]
	
	[row]
		[*][aimg height=2em]plugins/video.svg[/aimg]
		[*][aurl="plugins/flash.txt"]Adobe[sup]®[/sup] Flash[sup]®[/sup][/aurl]
	[/row]
	
	[row]
		[*][aimg height=2em]plugins/gaming.svg[/aimg]
		[*][aurl="plugins/shockwave.txt"]Shockwave[sup]®[/sup] Player[/aurl]
	[/row]
	
	[row]
		[*][aimg height=2em]plugins/gaming.svg[/aimg]
		[*][aurl="plugins/unity.txt"]Unity[sup]®[/sup] Web Player[/aurl]
	[/row]
	
	[row]
		[*][aimg height=2em]plugins/video.svg[/aimg]
		[*][aurl="plugins/widevine.txt"]Widevine[sup]®[/sup][/aurl]
	[/row]
	
	[row]
		[*][aimg height=2em]plugins/npactivex.png[/aimg]
		[*][aurl="plugins/npactivex.txt"]npactivex[/aurl]
	[/row]
[/table]

There are also some other smaller plugins, which do not have a dedicated page. You can get a list of all supported plugins by executing:

[code=bash]
pipelight-plugin --help
[/code]

Some of these plugins need to be "unlocked" first (this will create some extra file consuming some disk space). If you try to enable such a plugin, you will get an error tell you to execute

[code=bash]
sudo pipelight-plugin --unlock pluginname
[/code=bash]

first. If you do not longer need a specific plugin you can use  [code]--lock[/code] instead to remove the file again.

The following additional plugins (i.e. they were not mentioned yet) are supported as of version 0.2.7 and must be unlocked:

[table center nobox]
	[row][*] [b]Plugin[/b] [*] [b]Command[/b] [*] [b]Description[/b][/row]
	[row][*] Adobe[sup]®[/sup] Reader[sup]®[/sup] [*] [code]adobereader[/code] [*] Popular PDF viewer on Windows[/row]
	[row][*] Foxit[sup]®[/sup] PDF Reader [*] [code]foxitpdf[/code] [*] Another PDF viewer[/row]
	[row][*] Grandstream[sup]®[/sup] Plugin [*] [code]grandstream[/code] [*] Plugin to connect to IP cameras[/row]
	[row][*] Hikvision[sup]®[/sup] Plugin [*] [code]hikvision[/code] [*] Another plugin to connect to IP cameras[/row]
	[row][*] Roblox[sup]®[/sup] Plugin [*] [code]roblox[/code] [*] A web plugin to launch games created with Roblox[/row]
	[row][*] Vizzed Retro Game Room [*] [code]vizzedrgr[/code] [*] A game console emulator for old games[/row]
	[row][*] Viewright[sup]®[/sup] Caiway[sup]®[/sup] [*] [code]viewright-caiway[/code] [*] The ViewRight player in the Caiway edition (VOD service in NL)[/row]
	[row][*] Triangleplayer [*] [code]triangleplayer[/code] [*] Required to play the game "The Lost Titans"[/row]
	[row][*] Unity[sup]®[/sup] Web Player (64-bit) [*] [code]x64-unity3d[/code] [*] 64-bit version of the Unity Webplayer[/row]
	[row][*] Adobe[sup]®[/sup] Flash[sup]®[/sup] (64-bit) [*] [code]x64-flash[/code] [*] 64-bit version of Flash[/row]
[/table]

[section]Further steps and information[/section]

Some websites will show you an error that your operating system is not supported. To get around this problem, you will need to install an user agent switcher as described [aurl=useragent.txt]here[/aurl]. 

In case something does not work as expected take a look at our [aurl=../help/help.txt]help section[/aurl] which already contains solutions for the most common problems. In case none of the provided tips solves your problem, you will also find information about submitting questions or a bug report on this page.
