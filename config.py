# This config file is part of the DP-CMS system and contains all global settings

#-- IMAGES

IMAGE_TYPES 				= ['.jpeg', '.jpg', '.png', '.svg']
IMAGE_USE_PIL 				= False
IMAGE_COMMENT 				= "Pipelight Project"


#-- BBCODE

BBCODE_INTERNAL_HOSTNAMES 	= ("pipelight.net", "www.pipelight.net")

#-- CMSGEN

CMSGEN_DEFAULT_AUTHOR 		= "Pipelight Team"
CMSGEN_TIME_FORMAT 			= "%a, %d %b %Y %H:%M:%S %Z"

CMSGEN_WEBSITE_ROOT 		= "./cms"
CMSGEN_ARTICLE_SOURCE 		= "./source"

CMSGEN_STATIC_URL			= "/cms/"
CMSGEN_WEBSITE_URL 			= "/cms/"

CMSGEN_FAVICON_TYPE			= "image/png"
CMSGEN_FAVICON_URL 			= "f.png"

CMSGEN_VERSION				= "0.1.1"


#-- SITEMAP

SITEMAP_URL_PREFIX    		= "http://pipelight.net"


#-- RSS

RSS_FEED_NAME				= "Pipelight News Feed"
RSS_URL_PREFIX    			= "http://pipelight.net"

#-- SITEMAP

SITEMAP_INVALIDATE_URLS		= None

"""
SITEMAP_INVALIDATE_URLS 	=   [
									("www.google.com", "/webmasters/tools/ping?sitemap=%s"),
									("www.bing.com", "/webmaster/ping.aspx?siteMap=%s"),
									("submissions.ask.com", "/ping?sitemap=%s")
								]
"""